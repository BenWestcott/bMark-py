"""
@file      semantics.py
@copyright Benjamin Westcott, 2022
@brief     Semantic error checking and in-place optimization of the parse-tree
"""
import copy
import model
import tokens
import tree
import errors
import util

class SemanticsCTX:
	def __init__(self, config:model.Config, tr:tree.Tree, errs:list):
		self.config = config
		self.tree = tr
		self.errors = errs
	def child(self, tr:tree.Tree):
		c = copy.copy(self)
		c.tree = tr
		return c

"""Calls the appropriate semantic checking and transformation functions for the given tree."""
def semantics(c:SemanticsCTX) -> bool:
	def _int_route(c:SemanticsCTX) -> bool:
		if c.tree.node.type_.name in INTERNAL_ANALYZERS:
			return INTERNAL_ANALYZERS[c.tree.node.type_.name](c)
		else:
			ok = True
			for op in c.tree.node.type_.semantic_ops:
				ok &= SEMANTIC_OPS[op["name"]](c, op)
			c.tree.flags['A'] = True
			return ok
	
	if c.tree.flags['A']:
		raise RuntimeError("Attempt to analyze already analyzed tree: " + str(c.tree))
	ok = True
	if c.tree.node != tree.Root:
		ok = _int_route(c)
	else:
		for subtree in c.tree.by_flag('A', False):
			ok &= _int_route(c.child(subtree))
		c.tree.flags['A'] = True # Mark the root node when done
	return ok

"""Function for token types that don't need any semantic checks or transformations"""
def _default(c:SemanticsCTX) -> bool:
	success = True
	for subtree in c.tree.by_flag('A', False):
		success &= semantics(c.child(subtree))
	c.tree.flags['A'] = True
	return success

"""Helper that requires all children of a tree to be one of a given list of types."""
def _require_children_type(c:SemanticsCTX, types:set, id_:int) -> bool:
	good = True
	i = 0
	while i < len(c.tree.children):
		node = c.tree.children[i].node
		if node.type_ not in types:
			# Before erroring, check if it's just whitespace between items
			if node.type_ == c.config.type_text and tokens.WHITESPACE.match(node.lexeme):
				# Remove the whitespace
				c.tree.remove(i)
				continue
			# Actual disallowed node with content here
			c.errors.append(errors.CompileError(id_, errors.Semantic.INVALID_TOKEN_TYPE, node, \
				f"{c.tree.parent.node.str_msg()} accepts only {util.str_list(types)}"))
			good = False
		i += 1
	return good

"""Transforms the subtree into a collection of individual TEXT arguments"""
def _handle_args(c:SemanticsCTX) -> bool:
	"""Splits argument TEXT tokens on commas"""
	def _int_split(c:SemanticsCTX):
		arg_list = []
		pos_list = []
		for child in c.tree.children:
			new_args = child.node.lexeme.split(",")
			arg_list += new_args
			running_pos = child.node.pos.offset
			for arg in new_args:
				pos_list.append(running_pos)
				running_pos += len(arg) + 1
		c.tree.children = []
		for i in range(0, len(arg_list)):
			position = tokens.Position(pos_list[i], child.node.pos.lines)
			subtree = tree.Tree(tokens.Token(c.config.type_text, arg_list[i].strip(), position))
			subtree.flags['A'] = True
			c.tree.add(subtree)
	
	success = _default(c)
	success &= _require_children_type(c, {c.config.type_text}, "builtin.semantics.args")
	_int_split(c)
	c.tree.flags['A'] = True
	return success

"""Converts itself into a TEXT token."""
def _handle_delimiter(c:SemanticsCTX) -> bool:
	c.tree.node.type_ = c.config.type_text
	c.tree.flags['A'] = True
	return True

"""TEXT helper that removes tabs and newlines, inserting paragraph breaks at double-newlines."""
def _transform_whitespace(c:SemanticsCTX) -> list:
	no_tabs = c.tree.node.lexeme.translate({9:None}) # ASCII 9 = tab
	lines = no_tabs.split('\n')
	first_tree = tree.Tree(tokens.Token(c.config.type_text, "", c.tree.node.pos))
	first_tree.flags['A'] = True
	subtrees = [first_tree]
	
	offset = c.tree.node.pos.offset
	for i in range(0, len(lines)):
		line = lines[i]
		if line ==  "":
			if i in [0, len(lines)-1]:
				# Don't insert paragraph breaks before the start or after the end of a text block
				offset += len(line) + 1
				continue
			# Double newline; insert a paragraph break, and start a new text node
			position = tokens.Position(offset, c.tree.node.pos.lines)
			b = tree.Tree(tokens.Token(c.config.type_paragraph_break, "", position))
			b.flags['A'] = True
			subtrees.append(b)
			t = tree.Tree(tokens.Token(c.config.type_text, "", position))
			t.flags['A'] = True
			subtrees.append(t)
		else:
			# Add to the current text node
			node = subtrees[-1].node
			if len(node.lexeme) > 0:
				# Replace whitespace lost by stripping newlines
				node.lexeme += " "
			node.lexeme += line
		offset += len(line) + 1
	# If this text block ends in a linebreak and the next line is not part of the lines array due to being a non-text
	#   node, don't lose that whitespace
	if c.tree.node.lexeme[-1] == '\n':
		next_ = c.tree.next()
		if next_ and not c.tree.next().flags['O']:
			subtrees[-1].node.lexeme += " "
	
	# Remove the current node and insert the generated nodes
	loc = c.tree.index()
	parent = c.tree.parent # Save this because remove() unlinks
	c.tree.parent.remove(loc)
	for subtree in subtrees:
		# Skip empty text nodes (generated by starting/ending/sequential paragraph breaks)
		if subtree.node.lexeme != "":
			parent.add(subtree, loc)
			loc += 1
	return subtrees

"""TEXT helper that preserves intended whitespace"""
def _transform_whitespace_plain(c:SemanticsCTX) -> list:
	# For more doc see _transform_whitespace
	no_tabs = c.tree.node.lexeme.translate({9:None}) # ASCII 9 = tab
	lines = no_tabs.split('\n')
	subtrees = []
	offset = c.tree.node.pos.offset
	for i in range(0, len(lines)):
		if lines[i] != "":
			position = tokens.Position(offset, c.tree.node.pos.lines)
			t = tree.Tree(tokens.Token(c.config.type_text, lines[i], position))
			t.flags['A'] = True
			subtrees.append(t)
		if i != len(lines)-1:
			position = tokens.Position(offset, c.tree.node.pos.lines)
			b = tree.Tree(tokens.Token(c.config.token_types[tokens.LINEBREAK], "", position))
			b.flags['A'] = True
			subtrees.append(b)
		offset += len(lines[i]) + 1
	loc = c.tree.index()
	parent = c.tree.parent
	c.tree.parent.remove(loc)
	for subtree in subtrees:
		parent.add(subtree, loc)
		loc += 1
	return subtrees

"""Merges sequential text, strips tabs and single newlines, and inserts paragraph breaks at double newlines."""
def _handle_text(c:SemanticsCTX) -> bool:
	is_plain = False
	if c.tree.parent:
		if c.tree.parent.flags['P']:
			is_plain = True
		# Look an extra level above for a collection override if the parent is a group
		elif c.tree.parent.node != tree.Root and c.tree.parent.node.type_ == c.config.type_group:
			is_plain = c.tree.parent.parent.flags['P']
	if is_plain:
		_transform_whitespace_plain(c)
	else:
		_transform_whitespace(c)
	return True

"""Map of special handlers for internal tokens used by the main semantic analysis function"""
INTERNAL_ANALYZERS = {
	"args": _handle_args,
	"delim": _handle_delimiter,
	"group": _default,
	"text": _handle_text
}

def _op_default(c:SemanticsCTX, args:dict) -> bool:
	tr = c.tree
	if "index" in args:
		index = int(args["index"])
		tr = c.tree.children[index]
	return _default(c.child(tr))

def _op_remove_self(c:SemanticsCTX, args:dict) -> bool:
	c.tree.parent.remove(c.tree.index())
	return True

def _op_require_args(c:SemanticsCTX, args:dict) -> bool:
	fmt = args["format"]
	tr = c.tree.children[]
	# Argstr is a concatenation of all argument lexemes,
	# but with numbers replaced by '#' for brevity and simplicity of regexes
	argstr = ""
	for i, child in enumerate(tr.children):
		m = c.config.numeric.match(child.node.lexeme)
		if m and m.end() == len(child.node.lexeme):
			argstr += "#"
		else:
			argstr += child.node.lexeme
		if i != len(tr.children)-1:
			argstr += ','
	return re.match(fmt, argstr)

def _op_require_arg_count(c:SemanticsCTX, args:dict) -> bool:
	count = int(args["count"])
	tr = c.tree.children[0]
	badNode = None
	if len(tr.children) < count:
		badNode = tr.children[-1].node
	elif len(tr.children) > count:
		badNode = tr.children[count].node
	
	if badNode:
		id_ = c.config.error_names[args["id"]]
		c.errors.append(errors.CompileError(id_, errors.Semantic.INCORRECT_ITEM_COUNT, badNode, \
			f"{tr.parent.node.str_msg()} requires exactly {count} nodes, but {len(tr.children)} were found"))
		return False
	return True

def _op_require_args_numeric(c:SemanticsCTX, args:dict) -> bool:
	empty_permitted = True if args["empty_permitted"] == "1" else False
	good = True
	for tr in c.tree.children[0].children:
		if tr.node.lexeme == "" and empty_permitted:
			continue
		m = c.config.numeric.match(tr.node.lexeme)
		if not m or m.end() != len(tr.node.lexeme):
			id_ = c.config.error_names[args["id"]]
			c.errors.append(errors.CompileError(id_, errors.Semantic.INVALID_ARGUMENT, tr.node, \
				"not recognized as a numeric type; must be a decimal number optionally followed by '%' or 'pt'."))
			good = False
	return good

def _op_require_arg_values(c:SemanticsCTX, args:dict) -> bool:
	values = args["values"].split(';')
	for arg in c.tree.children[0].children:
		if arg.node.lexeme not in values:
			id_ = c.config.error_names[args["id"]]
			c.errors.append(errors.CompileError(id_, errors.Semantic.INVALID_ARGUMENT, arg.node, \
				f"arguments for {c.tree.node.str_msg()} must be one of {values}"))
			return False
	return True

def _op_require_parent_type(c:SemanticsCTX, args:dict) -> bool:
	type_names = args["types"].split(';')
	parent = c.tree.parent
	if not parent:
		raise RuntimeError("Requiring parent type of node with no parent")
	# If in a group, check the parent of the group instead
	if parent != tree.Root and parent.node.type_ == c.config.type_group and parent.parent:
		parent = parent.parent
	if parent.node.type_.name in type_names:
		return True
	else:
		types = []
		for name in type_names:
			types.append(str(c.config.type_by_name(name)))
		id_ = c.config.error_names[args["id"]]
		c.errors.append(errors.CompileError(id_, errors.Semantic.INVALID_CONTEXT, c.tree.node, \
			f"must only be in one of {types}, not {str(parent.node.type_)}"))
		return False

def _op_require_children_type(c:SemanticsCTX, args:dict) -> bool:
	tr = c.tree
	if "index" in args:
		index = int(args["index"])
		tr = c.tree.children[index]
	type_names = args["types"].split(';')
	types =[]
	for name in type_names:
		types.append(c.config.type_by_name(name))
	id_ = c.config.error_names[args["id"]]
	return _require_children_type(c.child(tr), types, id_)

# Map of XML operation names to function pointers for use by the main semantic analysis function
SEMANTIC_OPS = {
	"default": _op_default,
	"remove_self": _op_remove_self,
	"require_arg_count": _op_require_arg_count,
	"require_args_numeric": _op_require_args_numeric,
	"require_arg_values": _op_require_arg_values,
	"require_parent_type": _op_require_parent_type,
	"require_children_type": _op_require_children_type
}
