"""
@file      Test.py
@copyright Benjamin Westcott, 2022
@brief     Runs the full document generation workflow
"""
import sys
import model
import tokens
import scan
import parse
import tree
import semantics
import emit_html

# usage: python Test.py <scan|parse|semantics|emit>
# The argument indicates how far along the workflow runs before stopping

"""Set up flow control based on user input"""
class CmdOpts:
	def __init__(self):
		self.verbose = 0
		self.scan = False
		self.parse = False
		self.semantics = False
		self.emit = False
		self.source = open("data/test_full.txt")
		self.dest = open("data/converted.html", "w")
		self.debug_ir_parse = "data/ir_parse.xml"
		self.debug_ir_semantics = "data/ir_semantics.xml"
	def read_args(self):
		if "-v" in sys.argv:
			self.verbose = 1
		if "-vv" in sys.argv:
			self.verbose = 2
		if "scan" in sys.argv:
			self.scan = True
		if "parse" in sys.argv:
			self.parse = True
		if "semantics" in sys.argv:
			self.parse = True
			self.semantics = True
		if "emit" in sys.argv:
			self.parse = True
			self.semantics = True
			self.emit = True


"""Returns True if any double-links are mismatched"""
def find_broken_links(tr:tree.Tree) -> bool:
	if len(tr.children) == 0:
		return False
	broken = False
	for child in tr.children:
		broken |= find_broken_links(child)
		if id(child.parent) != id(tr):
			print("    between", tr.node, "and", child.node)
			broken = True
	return broken

"""Returns True and prints any tree nodes that were not analyzed"""
def find_not_analyzed(tr:tree.Tree) -> bool:
	if (not tr.flags['A']) and (not tr.flags['C']):
		idx = tr.index()
		print(f"    -> {tr.node} ({idx}) in")
		tr.flags['C'] = True
		return True
	if len(tr.children) == 0:
		return False
	for child in tr.children:
		if find_not_analyzed(child):
			if tr.node == tree.Root:
				print("    root node")
			else:
				idx = child.index()
				print(f"    child {idx} ({tr.children[idx].str_shallow()}) of {tr.str_shallow()} in")
			return True
	return False


"""Runs the parser stage of the workflow, producing a raw parse tree"""
def do_parse(prs:parse.Parser, opts:CmdOpts) -> bool:
	success = prs.parse()
	print("Parse status:", success)
	print("Parse errors:")
	for err in prs.errors:
		print("   ", err)
	if opts.verbose == 1:
		print("Parse tree (shallow):", prs.tree.str_shallow())
	elif opts.verbose == 2:
		out = open(opts.debug_ir_parse, "w")
		out.write(prs.tree.to_xml(pretty=True))
		out.close()
		print("Parse tree written to", opts.debug_ir_parse)
	print("Broken links:")
	success &= not find_broken_links(prs.tree)
	return success


"""Runs the semantic verification and optimization stage of the workflow, modifying the parse tree"""
def do_semantics(config:model.Config, tr:tree.Tree, opts:CmdOpts) -> bool:
	print("=== SEMANTICS ===")
	ctx = semantics.SemanticsCTX(config, tr, [])
	success = semantics.semantics(ctx)
	print("Analysis status:", success)
	print("Analysis errors:")
	for err in ctx.errors:
		print("\t", err)
	if opts.verbose == 1:
		print("Transformed tree (shallow):", tr.str_shallow())
	elif opts.verbose == 2:
		out = open(opts.debug_ir_semantics, "w")
		out.write(tr.to_xml(pretty=True))
		out.close()
		print("Transformed tree written to", opts.debug_ir_semantics)
	print("Broken links:")
	success &= not find_broken_links(tr)
	if success:
		print("Unanalyzed nodes:")
		while find_not_analyzed(tr):
			success = False
			print("    ---")
	return success

"""Runs the emission stage of the workflow, producing a document file"""
def do_emission(config:model.Config, tr:tree.Tree, opts:CmdOpts) -> None:
	print("=== EMISSION ===")
	emit_html.emit(config, opts.dest, tr)
	print("Emitted")


"""Opens an input file and runs the workflow"""
def do_workflow(opts:CmdOpts):
	config_str = None
	with open("config.xml") as f:
		config_str = f.read()
	config = model.Config(config_str)
	scn = scan.BufferedScanner(opts.source, config, 1, 1)
	if opts.scan:
		while scn.has_next():
			print(scn.next())
	if opts.parse:
		print("=== PARSE ===")
		prs = parse.Parser(config, scn)
		ok = do_parse(prs, opts)
		if not ok:
			print("Compilation stopped due to parsing errors.")
		elif opts.semantics:
			ok = do_semantics(config, prs.tree, opts)
			if not ok:
				print("Compilation stopped due to semantic analysis errors.")
			elif opts.emit:
				do_emission(config, prs.tree, opts)

opts = CmdOpts()
opts.read_args()
do_workflow(opts)
