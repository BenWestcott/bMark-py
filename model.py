"""
@file      model.py
@copyright Benjamin Westcott, 2022
@brief     Provides data structures representing the language model
"""
import copy
import re
import xml.etree.ElementTree as xmltree
import tokens

INTERNAL_TOKEN_COUNT = 6

# TODO: make these configured in the XML
SUFFIX_INPUT_EOF = "E"
SUFFIX_INPUT_NONE = "I"
SUFFIX_INPUT_OPEN = "U"

class Config:
	def __init__(self, xml:str):
		self.max_len = 0 # Longest keyword (including preceeding !)
		self.trigger = None
		self.numeric = None
		self.numpart = None
		self.any_delimiter = None
		self.arg_delimiters = []
		self.content_delimiters = []
		self.token_types = []
		self.error_names = {}
		
		# Set xml_tree, error checking
		xml_tree = xmltree.XML(xml)
		Config._validate_xml(xml_tree)
		
		# Assign main config attributes
		self.trigger = re.compile(xml_tree[0].get("val"))
		self.numeric = re.compile(xml_tree[1].get("numeric"))
		self.numpart = re.compile(xml_tree[1].get("numpart"))
		arg1 = xml_tree[2].get("pre")
		self.arg_delimiters.append(arg1)
		arg2 = xml_tree[2].get("post")
		self.arg_delimiters.append(arg2)
		content1 = xml_tree[3].get("pre")
		self.content_delimiters.append(content1)
		content2 = xml_tree[3].get("post")
		self.content_delimiters.append(content2)
		self.any_delimiter = re.compile("[\\"+arg1+"\\"+arg2+"\\"+content1+"\\"+content2+"]")
		
		# Generate token types
		for token_node in xml_tree[4]:
			token_type = self._add_token_type(token_node)
			if len(token_node) != 0:
				self._add_token_type_ops(token_type, token_node[0], token_node[1])
		
		# Add error names
		for error_name in xml_tree[5]:
			id_ = error_name.get("id")
			name = error_name.get("name")
			self.error_names[id_] = name
		
		# Shortcuts for built-in token types
		self.type_undefined = self.token_types[0]
		self.type_arguments = self.token_types[1]
		self.type_group = self.token_types[2]
		self.type_text = self.token_types[3]
		self.type_paragraph_break = self.token_types[4]
		self.type_delimiter = self.token_types[5]
	
	@classmethod
	def _validate_xml(cls, xml_tree) -> None:
		if xml_tree.tag != "config":
			raise ValueError("Provided language model XML is not a config XML")
		for config_sub in xml_tree:
			if config_sub.tag == "tokens":
				xml_tree = config_sub
				break
		if not xml_tree:
			raise ValueError("Language model XML contains no token configuration")
		if (
				len(xml_tree) < 6
				or xml_tree[0].tag != "trigger"
				or xml_tree[1].tag != "num_format"
				or xml_tree[2].tag != "arg_delimiters"
				or xml_tree[3].tag != "content_delimiters"
				or xml_tree[4].tag != "token_types"
				or xml_tree[5].tag != "error_names"
				or not xml_tree[0].get("val")
				or not xml_tree[1].get("numeric")
				or not xml_tree[1].get("numpart")
				or not xml_tree[2].get("pre")
				or not xml_tree[2].get("post")
				or not xml_tree[3].get("pre")
				or not xml_tree[3].get("post")
			):
			raise ValueError("Invalid language model XML")
		if len(xml_tree[4]) < INTERNAL_TOKEN_COUNT:
			raise ValueError("Language model XML is missing built-in types")
		for i, token_node in enumerate(xml_tree[4]):
			name = token_node.tag
			if i < INTERNAL_TOKEN_COUNT:
				if len(token_node) != 0:
					raise RuntimeError(f"Language model XML cannot modify behavior of internal token type \"{name}\"")
				continue
			else:
				if len(token_node) != 2:
					raise ValueError(f"Language model XML must specify parsing and semantics of token type \"{name}\"")
			if token_node[0].tag != "parse":
				raise ValueError(f"First XML subelement of token type \"{name}\" must be \"parse\"")
			if token_node[1].tag != "semantics":
				raise ValueError(f"Second XML subelement of token type \"{name}\" must be \"semantics\"")
		has_builtin_semantics_args = False
		for error_node in xml_tree[5]:
			if error_node.tag != "error":
				raise ValueError(f"Error names group must only contain errors, not \"{error_node.tag}\"")
			if not error_node.get("id") or not error_node.get("name"):
				raise ValueError("Error name specifier must have \"id\" and \"name\" options set")
			if error_node.get("id") == "builtin.semantics.args":
				has_builtin_semantics_args = True
		if not has_builtin_semantics_args:
			raise ValueError("Error name for builtin.semantics.args not specified")
	
	def _add_token_type(self, xml_node) -> tokens.TokenType:
		regex_str = xml_node.get("regex", xml_node.tag)
		# Don't add a regex if explicitly specified as empty
		regex = None
		if len(regex_str) != 0:
			regex = re.compile(f"{self.trigger.pattern}{regex_str}")
			if len(regex.pattern) > self.max_len: # Requires all keyword regexes to be trivial
				self.max_len = len(regex.pattern)
		pretty = xml_node.get("pretty", xml_node.tag)
		token = tokens.TokenType(regex, xml_node.tag, pretty)
		self.token_types.append(token)
		return token
	
	def _add_token_type_ops(self, token_type:tokens.TokenType, parse_items, semantic_items)  -> None:
		def _add_op(item, token_type:tokens.TokenType, attr:str, part:str):
			if item.tag == "copy":
				# token_type.attr = type_by_name(item.from).attr
				setattr(token_type, attr, getattr(self.type_by_name(item.get("from")), attr))
				return
			item_dict = copy.copy(item.attrib)
			item_dict["name"] = item.tag
			if "id" in item_dict:
				item_dict["id"] = f"{token_type.name}.{part}." + item_dict["id"]
			getattr(token_type, attr).append(item_dict)
		
		for item in parse_items:
			_add_op(item, token_type, "parse_ops", "parse")
		for item in semantic_items:
			_add_op(item, token_type, "semantic_ops", "semantics")
	
	def type_by_name(self, name:str):
		for token_type in self.token_types:
			if token_type.name == name:
				return token_type
		raise RuntimeError(f"Undefined token type: \"{name}\"")
