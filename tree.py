"""
@file      tree.py
@copyright Benjamin Westcott, 2022
@brief     Doubly linked annotated parse tree
"""
import xml.etree.ElementTree as xmltree
import xml.dom.minidom as minidom
import model
import tokens
import util


"""Class placed in the node parameter at the root of a parse tree."""
class Root: # Test with tree.node == Root
	@classmethod
	def str_msg(cls):
		return "root node"

"""Doubly-linked tree structure for organizing tokens."""
class Tree:
	def __init__(self, node:tokens.Token=None, parent=None, children:list=None):
		self.parent = parent
		self.node = node
		self.children = children
		if children is None:
			# Quirk workaround: default arguments are shared, not constructed each time
			# Initializing to an empty list in the args gives everyone a pointer to the same children list
			self.children = []
		# Flags
		self.flags = {
			'A': False, # Analyzed
			'C': False, # Checked
			'R': False, # Removed from the parse tree
			'O': False, # Out-of-line element
			'E': False, # Inverted escape behavior
			'P': False  # Plaintext / skip text transformation
		}
	
	def to_xml(self, linebreaks:list=None, pretty:bool=False) -> str:
		xml = xmltree.ElementTree()
		builder = xmltree.TreeBuilder()
		if linebreaks is not None:
			lbstr = ';'.join(map(str, linebreaks))
			builder.start("newlines", {"offsets":lbstr})
			self._to_xml(builder)
			builder.end("newlines")
		else:
			self._to_xml(builder)
		minified = xmltree.tostring(builder.close(), encoding='utf-8')
		if pretty:
			parsed = minidom.parseString(minified)
			return parsed.toprettyxml()
		else:
			return minified
	def _to_xml(self, builder:xmltree.TreeBuilder) -> None:
		attributes = {}
		flags = self.str_flags()
		if flags != "":
			attributes["flags"] = flags
		tag = ""
		if self.node == Root:
			tag = "root"
		else:
			tag = self.node.type_.name
			if self.node.lexeme != "":
				attributes["lexeme"] = self.node.lexeme
			if self.node.pos.offset != -1:
				attributes["offset"] = str(self.node.pos.offset)
		builder.start(tag, attributes)
		for child in self.children:
			child._to_xml(builder)
		builder.end(tag)
	
	"""Doubly links the given subtree as a child of this tree."""
	def add(self, child, pos:int=-1) -> None:
		if pos == -1:
			self.children.append(child)
		else:
			self.children.insert(pos, child)
		child.parent = self
		child.flags['R'] = False
	
	"""Unlinks, removes, and returns the subtree at the given location."""
	def remove(self, pos:int):
		tree = self.children.pop(pos)
		tree.parent = None
		tree.flags['R'] = True
		return tree
	
	"""Constructs a subtree for a token and make it a child of this tree."""
	def add_token(self, token:tokens.Token) -> None:
		self.add(Tree(token))
	
	"""Finds the index of this tree within the parent's list of children"""
	def index(self) -> int:
		siblings = self.parent.children
		sid = id(self)
		for i in range(0, len(siblings)):
			if sid == id(siblings[i]):
				return i
		raise RuntimeError("Corrupted parse tree")
	
	"""Finds the next node in the tree; will traverse upwards but not downwards"""
	def next(self):
		node = self
		# If the last node at this level, go up
		while node.parent and id(node) == id(node.parent.children[-1]):
			node = node.parent
		# Backed all the way up to the root node looking for a next means we're the last
		if not node.parent:
			return None
		return node.parent.children[node.index()+1]
	
	"""Generator for unvisited children. Robust to tree mutations between calls."""
	def by_flag(self, flag:str, val):
		excluded = set()
		i = 0
		while len(excluded) < len(self.children):
			child = self.children[i]
			if child.flags[flag] == val:
				yield child
				if child.flags[flag] == val and not child.flags['R']:
					raise RuntimeError("Flag " + flag + " not set after child was yielded:\n\t" + str(child))
			if child.flags[flag] != val:
				excluded.add(id(child))
			i = (i + 1) % len(self.children)
		return
	
	def __hash__(self) -> int:
		return hash((self.node, id(self.parent), id(self.children), self.flags.values()))
	"""Note: compares parents by ID to avoid infinite recursion."""
	def __eq__(self, other) -> bool:
		return type(other) == Tree and self.node == other.node and self.children == other.children and \
			id(self.parent) == id(other.parent)
	def __str__(self) -> str:
		return f"<{self.str_flags()}:{self.node}|{util.str_list(self.children)[1:-1]}>"
	def __repr__(self) -> str:
		return f"Tree.Tree({self.node!r}, id:{id(self.parent)}, id:{id(self.children)})"
	def str_flags(self) -> str:
		flagStr = ""
		for key in self.flags.keys():
			if self.flags[key]:
				flagStr += key
		return flagStr
	def str_shallow(self) -> str:
		s = f"<{self.str_flags()}:{self.node}|"
		for child in self.children:
			s += f"{self.str_flags()}:{child.node}:{len(child.children)}, "
		s += ">"
		return s


def from_xml(config:model.Config, xml:str) -> Tree:
	xmlTree = xmltree.XML(xml)
	linebreaks = []
	if xmlTree.tag == "newlines":
		offsets = xmlTree.get("offsets", "").split(";")
		linebreaks = list(map(int, offsets))
		return _from_xml(config, xmlTree[0], linebreaks)
	else:
		return _from_xml(config, xmlTree, [])

def _from_xml(config:model.Config, xmlTree:xmltree.Element, linebreaks:list) -> Tree:
	node = None
	if xmlTree.tag == "root":
		node = Root
	else:
		offset = int(xmlTree.get("offset", "-1"))
		position  = tokens.Position(offset, linebreaks)
		tokenType = config.type_by_name(xmlTree.tag)
		lexeme = xmlTree.get("lexeme", "")
		node = tokens.Token(tokenType, lexeme, position)
	self = Tree(node)
	flagStr = xmlTree.get("flags", "")
	for flag in flagStr:
		self.flags[flag] = True
	for i in range(0, len(xmlTree)):
		subtree = _from_xml(xmlTree[i], linebreaks)
		self.add(subtree)
	return self
