"""
@file      parsedata.py
@copyright Benjamin Westcott, 2022
@brief     Implements a simple recursive descent parser
"""
import copy
import model
import tokens
import scan
import tree
import errors

"""Holds relevant information needed by parsers, with a method to assist recursion."""
class ParseCtx:
	def __init__(self, config:model.Config, scan:scan.BufferedScanner, errors:list, parent:tree.Tree, continue_:bool):
		self.config = config
		self.scan = scan
		self.errors = errors
		self.parent = parent
		self.tree = None
		self.continue_ = continue_
	"""Returns a new context that is a copy of this one, but with the parent tree set to the given argument."""
	def child(self, tr:tree.Tree=None):
		c = copy.copy(self)
		if tr is None:
			assert c.tree is not None
			c.parent = c.tree
		else:
			c.parent = tr
		c.tree = None
		return c


"""Delegates parsing the next token to the appropriate function"""
def parse(c:ParseCtx) -> bool:
	if not c.scan.has_next():
		return False
	token = c.scan.peek()
	if token.type_.name in INTERNAL_PARSERS:
		return INTERNAL_PARSERS[token.type_.name](c)
	else:
		ok = True
		c.tree = tree.Tree(c.scan.next(), c.parent)
		for op in token.type_.parse_ops:
			ok &= PARSE_OPS[op["name"]](c, op)
		if ok:
			c.parent.add(c.tree)
		return ok

# == PARSER COMPONENTS ==
# These are helper functions for the parsers
# For consistency reasons, these should be called using a child context like a normal parser,
#  but _expect_delim() also works fine using the calling function's local context

"""Returns the next token iff it is the expected delimiter. Consumes the token if true."""
def _expect_delim(c:ParseCtx, delim:str) -> tokens.Token:
	if not c.scan.has_next():
		return None
	token = c.scan.peek()
	if token.type_ == c.config.type_delimiter and token.lexeme == delim:
		c.scan.next()
		return token
	else:
		return None

"""Parser component that runs until the context ends. Adds an error and returns false if the brace is not closed.
   tokens are inserted into a subtree of the appropriate type. Always adds an ARGUMENTS or GROUP subtree,
     it is just left empty on failure. Constant child counts and positions are helpful for the lexer."""
def _parse_input(c:ParseCtx, is_args:bool, optional:bool, id_:str) -> bool:
	assert type(id_) == str
	# Variable setup
	subgroup, lexemes, etype, emsg = None, None, None, None
	if is_args:
		subgroup = c.config.type_arguments
		lexemes = c.config.arg_delimiters
		etype = errors.Parse.EXPECTED_PAREN
		emsg = "arguments required"
	else:
		subgroup = c.config.type_group
		lexemes = c.config.content_delimiters
		etype = errors.Parse.EXPECTED_BRACE
		emsg = "content block required"
	# Handle EOF
	if not c.scan.has_next():
		c.parent.add_token(tokens.Token(subgroup, "", c.scan.peek(-1).pos))
		if not optional:
			# Encountered EOF, but input was marked as required
			c.errors.append(errors.CompileError(id_ + model.SUFFIX_INPUT_EOF, etype, c.scan.peek(-1), emsg))
		return optional
	# Get current input data
	token = c.scan.peek()
	subtree = tree.Tree(tokens.Token(subgroup, "", token.pos), c.parent)
	subctx = c.child(subtree)
	delim = _expect_delim(c, lexemes[0])
	# Handle when next token is not the expected
	if not delim:
		c.parent.add(subtree)
		if not optional:
			# Input marked as required, but the next token was not the start of input
			c.errors.append(errors.CompileError(id_ + model.SUFFIX_INPUT_NONE, etype, token, emsg))
		return optional
	# Consume input until end delimiter
	while not _expect_delim(subctx, lexemes[1]):
		if not c.scan.has_next():
			# EOF encountered before closing delimiter
			etype = errors.Parse.UNCLOSED_PAREN if is_args else errors.Parse.UNCLOSED_BRACE
			c.errors.append(errors.CompileError(id_ + model.SUFFIX_INPUT_OPEN, etype, delim))
			c.parent.add(subtree)
			return False
		if not parse(subctx):
			# Parsing failed, but there were tokens available to parse
			# This means the failure was in the child, not us, so no need to add an error
			c.parent.add(subtree)
			return False
	# Done!
	c.parent.add(subtree)
	return True

# == INTERNAL PARSERS ==
# These implement built-in behaviors for internal token types that cannot be changed by the config XML.
# _parse_text is called directly for packaging up keyword arguments,
#   but otherwise these should only be called through the main parse function.

"""Take no arguments and has a standalone semantic meaning"""
def _parse_delim(c:ParseCtx) -> bool:
	tkn = c.scan.next()
	c.parent.add_token(tkn)
	return True

"""TEXT. Special handling for consuming sequential text blocks and escaped tokens."""
def _parse_text(c:ParseCtx) -> bool:
	tkn = c.scan.next()
	# If the next token is in an escaping configuration such that it should be interpreted as text,
	# consume it and directly add it to this text node
	while c.scan.has_next():
		if c.scan.peek().type_ == c.config.type_text:
			# Always merge sequential text
			tkn.lexeme += c.scan.next().lexeme
		else:
			# Merge other tokens conditionally on escaping
			backslash = False # Is the next token escaped?
			if len(tkn.lexeme) > 0:
				if tkn.lexeme[-1] == "\\":
					backslash = True
					tkn.lexeme = tkn.lexeme[:-1]
			inverted = c.parent.parent != None and c.parent.parent.flags['E'] # Is escape behavior inverted?
			# Normal -> add when backslash, inverted -> add when not backslash
			add_next = backslash if not inverted else not backslash
			if add_next:
				tkn.lexeme += c.scan.next().lexeme
			else:
				break
	if tkn.lexeme != "":
		c.parent.add_token(tkn)
	return True


"""Map of special handlers for internal tokens used by the main parse function"""
INTERNAL_PARSERS = {
	"delim": _parse_delim,
	"text": _parse_text
}

# == PARSE OPERATIONS ==
# Individual parsing operations that are invoked according to a token's configuration in the XML

"""Sets the flag corresponding to each character in the "flags" argument"""
def _op_set_flags(c:ParseCtx, args:dict) -> bool:
	for flag in args["flags"]:
		c.tree.flags[flag] = 1
	return True

"""Runs _parse_input, passing along is_args, optional, and id.
Additionally, min and max can be used to specify a variable-length sequence of inputs of the same type."""
def _op_parse_input(c:ParseCtx, args:dict) -> bool:
	is_args = True if args["is_args"] == "1" else False
	first_delim = c.config.arg_delimiters[0] if is_args else c.config.content_delimiters[0]
	optional = True if args["optional"] == "1" else False
	id_ = c.config.error_names[args["id"]]
	# Fully inclusive range, default to exactly one; specify -1 max for unlimited
	min_count = int(args["min"]) if "min" in args else 1
	max_count = int(args["max"]) if "max" in args else 1
	i = 0
	while i != max_count and c.scan.has_next() and \
			c.scan.peek().type_ == c.config.type_delimiter and c.scan.peek().lexeme == first_delim:
		if not _parse_input(c.child(), is_args, optional, id_):
			return False
		i += 1
	return i >= min_count

"""Shorthand for calling _parse_input for a mandatory content block."""
def _op_single_block(c:ParseCtx, args:dict) -> bool:
	id_ = c.config.error_names[args["id"]]
	return _parse_input(c.child(), False, False, id_)

# Map of XML operation names to function pointers for use by the main parse function
PARSE_OPS = {
	"set_flags": _op_set_flags,
	"parse_input": _op_parse_input,
	"single_block": _op_single_block,
}
