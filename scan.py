"""
@file      scan.py
@copyright Benjamin Westcott, 2022
@brief     Tokenizer specialized to a marker-based language format
"""
from collections import deque
import re
import model
import tokens
import util

# == Conceptual Overview ==
# - The file is read in chunks
# - Chunk alignment is adjusted in events of tokens potentially being split across chunks
# - The chunk is scanned for the trigger regex, and trigger occurrences are matched to specific tokens
# - The chunk is scanned for any delimiters, regardless of location
# - Found token and delimiter locations are turned into full token+lexeme objects and yielded while walking through the
#   chunk a second time
# - Characters between keywords and delimiters are collected into text tokens
# - The multi-pass system may be unoptimal for performance, but greatly reduces the complexity of flow control as well
#   as the quantity of repeated similar code blocks. This may be improved in the future.
#
# == Call Tree ==
# tokens
# |- _adv
# |- _find_tokens
# |  |- _find_keywords
# |  \- _find_delimiters
# \- _realign / _adv

_CHUNK_MAX = 2**64

"""A region of interest in the input buffer; either a token or a realignment request."""
class _Match:
	def __init__(self, start:int, end:int, type_:tokens.TokenType, realign_to:int=-1):
		self.start = start
		self.end = end
		self.type_ = type_
		self.needs_realign = False # Represents a realignment request rather than a token
		self.realign_to = realign_to
		if realign_to != -1:
			self.needs_realign = True
	"""Alternate constructor for realignment requests."""
	@classmethod
	def realign(cls, offset:int):
		# Set start position to a high value as it is used for sorting match objects.
		# Guaranteeing the realign request is at the end of the match list simplifies logic.
		return cls(_CHUNK_MAX, 0, None, offset)
	
	def __eq__(self, other) -> bool:
		return self.start == other.start and self.end == other.end and \
			self.type_ == other.type_ and self.realign_to == other.realign_to
	def __str__(self) -> str:
		if self.needs_realign:
			return f"REALIGN({self.realign_to})"
		else:
			return f"{self.type_.name}({self.start}:{self.end})"
	def __repr__(self) -> str:
		return f"_Match({self.start}, {self.end}, {self.type_!r}, {self.realign_to})"

"""Base-level scanner providing a token generator."""
class Scanner:
	def __init__(self, doc, config:model.Config, chunk_size:int=512, debug:bool=False):
		if not (util.has_method(doc, 'read') or util.has_method(doc, 'tell')):
			raise ValueError("Scanner document must support read and tell methods")
		if not util.has_data(doc, 'mode') or 'b' in doc.mode:
			raise ValueError("Scanner document must not be opened in binary mode")
		self.doc = doc
		self.config = config
		self.debug = debug
		if chunk_size < config.max_len:
			raise ValueError(f"Scanner chunk size {chunk_size} is shorter than longest token ({tokens.MAX_LEN})")
		if chunk_size >= _CHUNK_MAX:
			raise ValueError(f"Scanner chunk size {chunk_size} must be smaller than the internal limit {_CHUNK_MAX}")
		self.chunk_size = chunk_size
		self._chunk = ""
		self._chunk_origin = 0
		self._lines = []
	
	def __str__(self) -> str:
		return f"Scanner<{self.doc}>"
	def __repr__(self) -> str:
		return f"Scanner({self.doc!r}, {self.debug}, {self.chunk_size})"
	
	"""Print if debug flag is set."""
	def _dbg(self, msg:str) -> None:
		if self.debug:
			print("DEBUG: [SCAN] " + msg)
	
	"""Go to next chunk."""
	def _adv(self) -> None:
		self._chunk_origin = self.doc.tell()
		self._chunk = self.doc.read(self.chunk_size)
		i = self._chunk.find('\n', 0)
		while i != -1:
			self._lines.append(i + self._chunk_origin)
			i = self._chunk.find('\n', i+1)
	
	"""Advance chunk boundary."""
	def _realign(self, chunk_offset:int) -> None:
		#                   pos -v
		#    ...--|--this-chunk--|--...
		# origin -^
		#         ^-offset-^
		remainder = self._chunk[chunk_offset:]
		new_origin = self.doc.tell()
		new = self.doc.read(chunk_offset)
		self._chunk = remainder + new
		self._chunk_origin += chunk_offset
		i = new.find('\n', 0)
		while i != -1:
			self._lines.append(i + new_origin)
			i = self._chunk.find('\n', i+1)
	
	"""Returns a list of all keyword _Matches in the chunk."""
	def _find_keywords(self) -> list:
		keywords = []
		for trigger in re.finditer(self.config.trigger, self._chunk):
			pos = trigger.start()
			# Request realign when there is a chance data after the end of this chunk is relevant to this token
			has_more = len(self._chunk) == self.chunk_size
			if has_more and pos >= len(self._chunk) - self.config.max_len:
				keywords.append(_Match.realign(pos))
				break
			subchunk = self._chunk[pos:pos+self.config.max_len]
			for token_type in self.config.token_types:
				if not token_type.regex:
					continue
				match = token_type.regex.match(subchunk)
				if not match:
					continue
				keywords.append(_Match(pos+match.start(), pos+match.end(), token_type))
				break
		self._dbg(f"Found {len(keywords)} keywords in chunk")
		return keywords
	
	"""Returns a list of all delimiter indices in the chunk."""
	def _find_delimiters(self) -> list:
		delims = []
		for delim in re.finditer(self.config.any_delimiter, self._chunk):
			delims.append(delim.start())
		self._dbg(f"Found {len(delims)} delimiters in chunk")
		return delims
	
	"""Combines findKeywords and findDelimiters into an indexed dict."""
	def _find_tokens(self) -> dict:
		keywords = self._find_keywords()
		delimiters = self._find_delimiters()
		protos = {}
		for kw in keywords:
			protos[kw.start] = kw
		for dl in delimiters:
			protos[dl] = self._chunk[dl]
		self._dbg("Proto-tokens: " + util.str_dict_sorted(protos))
		return protos
	
	"""Scanning logic (generator function)."""
	def tokens(self) -> tokens.Token:
		self._adv()
		# File content loop
		while self._chunk:
			if len(self._chunk) > 103:
				self._dbg(f"Read chunk: {self._chunk[0:50]!r}...{self._chunk[-50:-1]!r}")
			else:
				self._dbg(f"Read chunk: {self._chunk!r}")
			pos = 0 # Offset within this chunk
			realign = None
			protos = self._find_tokens()
			for idx in sorted(protos.keys()):
				proto = protos[idx]
				# Handle realignment request
				if type(proto) == _Match and proto.needs_realign:
					realign = proto.realign_to
					continue
				# Handle real proto-tokens
				if idx > pos:
					# Text between the end of the previous token and the start of this one
					position = tokens.Position(pos + self._chunk_origin, self._lines)
					yield tokens.Token(self.config.type_text, self._chunk[pos:idx], position)
					pos = idx
				if type(proto) == str:
					# This token (delimiter)
					position = tokens.Position(pos + self._chunk_origin, self._lines)
					yield tokens.Token(self.config.type_delimiter, proto, position)
					pos = idx + 1
				else: # type == _Match
					# This token (keyword)
					position = tokens.Position(proto.start + self._chunk_origin, self._lines)
					yield tokens.Token(proto.type_, self._chunk[proto.start:proto.end], position)
					pos = proto.end
			# Yield text from end of last token to end of chunk, then realign/advance
			if realign:
				if realign > pos:
					position = tokens.Position(pos + self._chunk_origin, self._lines)
					yield tokens.Token(self.config.type_text, self._chunk[pos:realign], position)
				self._dbg(f"Realign of {proto.realign_to - len(self._chunk)} requested")
				self._realign(realign)
			else:
				if pos < len(self._chunk):
					position = tokens.Position(pos + self._chunk_origin, self._lines)
					yield tokens.Token(self.config.type_text, self._chunk[pos:], position)
				self._adv()
			self._dbg("End of chunk")
		self._dbg("End of file")
		return

"""Buffering wrapper for Scanner providing lookahead and lookbehind."""
class BufferedScanner:
	def __init__(self, document, config:model.Config, lookahead:int, lookbehind:int=0, chunk_size:int=512, debug:bool=False):
		self.consumed = 0
		if lookahead < 0 or lookbehind < 0:
			raise ValueError("BufferedScanner look ahead/behind values must be greater than zero")
		self.lookahead = lookahead
		self.lookbehind = lookbehind
		self._bufsize = lookahead + lookbehind + 1
		self._scanner = Scanner(document, config, chunk_size, debug)
		self._buffer = deque()
		self._stream = self._scanner.tokens()
		# Fill the buffer
		padding = tokens.Token(self._scanner.config.type_undefined, "")
		while len(self._buffer) < self.lookbehind:
			self._buffer.append(padding)
		while len(self._buffer) < self._bufsize and self._enq():
			pass
	
	"""Adds a token to the right side of the buffer. Does not push items off the other end. Returns False at EOF."""
	def _enq(self) -> bool:
		n = None
		try:
			n = next(self._stream)
		except StopIteration:
			return False
		self._buffer.append(n)
		return True
	
	"""Removes and returns a token from the left side of the buffer."""
	def _deq(self) -> tokens.Token:
		token = self._buffer[self.lookbehind]
		self._buffer.popleft()
		self.consumed += 1
		return token
	
	"""Shifts the buffer forward by one, and returns the new current token. Errors when no tokens are available."""
	def next(self) -> tokens.Token:
		if not self.has_next():
			raise IndexError("No more tokens available")
		next_token = self._deq()
		self._enq()
		if self._scanner.debug:
			print("DEBUG: [BUFSCAN] Gave token " + str(next_token))
		return next_token
	
	"""Returns the token at the given offset relative to the current token. 0 is the next token."""
	def peek(self, offset:int=0) -> tokens.Token:
		idx = offset + self.lookbehind
		if idx < 0:
			raise IndexError("peek offset before start of buffer")
		if idx >= len(self._buffer):
			msg = "peek offset after end of buffer" if idx >= self._bufsize else "peek offset in unfilled buffer region"
			raise IndexError(msg)
		return self._buffer[idx]
	
	"""Returns the number of tokens in the lookahead buffer."""
	def available(self) -> int:
		return len(self._buffer) - self.lookbehind
	
	"""True iff there are tokens in the lookahead buffer."""
	def has_next(self) -> bool:
		return len(self._buffer) > self.lookbehind
