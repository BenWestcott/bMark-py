"""
@file      testsemantics.py
@copyright Benjamin Westcott, 2022
@brief     Functionality tests for Parse.py, Tree.py, and errors.py
"""
import unittest
import sys
sys.path.append('..')
import semantics
import tree
import tokens
import errors


def _assert_flags(tc:unittest.TestCase, tree:tree.Tree, flags:str):
	have = sorted([key for key in tree.flags.keys() if tree.flags[key]])
	need = sorted(flags)
	tc.assertEqual(have, need)
	
def _assert_good(tc:unittest.TestCase, tree:tree.Tree):
	ok, errs = semantics.semantics(tree)
	tc.assertTrue(ok)
	tc.assertEqual(errs, [])

class TestFatalerrors(unittest.TestCase):
	def test_analyzed(self):
		tr = tree.Tree()
		tr.flags['A'] = True
		self.assertRaises(RuntimeError, semantics.semantics, tr)
	def test_parentNone(self):
		tr = tree.Tree()
		self.assertRaises(RuntimeError, semantics._require_parent_type, tr, [], {}, 0)
		parent = tree.Tree(tokens.Token(tokens.TokenType.GROUP, ""))
		parent.add(tr)
		self.assertRaises(RuntimeError, semantics._require_parent_type, tr, [], {}, 0)

class TestDelegation(unittest.TestCase):
	def test_root_node_empty(self):
		tr = tree.Tree(tree.Root)
		_assert_good(self, tr)
		self.assertEqual(len(tr.children), 0)
		_assert_flags(self, tr, 'A')
	def test_root_node(self):
		tr = tree.from_xml('<root><b/><t/></root>')
		_assert_good(self, tr)
		_assert_flags(self, tr, 'A')
		self.assertEqual(len(tr.children), 2)
		ch1 = tr.children[0]
		ch2 = tr.children[1]
		self.assertEqual(ch1.node.type_, tokens.TokenType.LINEBREAK)
		self.assertEqual(ch2.node.type_, tokens.TokenType.TAB)
		_assert_flags(self, ch1, 'A')
		_assert_flags(self, ch2, 'A')
	def test_normal_node(self):
		tok = tokens.Token(tokens.TokenType.LINEBREAK, "!b")
		tr = tree.Tree(tok)
		_assert_good(self, tr)
		_assert_flags(self, tr, 'A')
		self.assertEqual(tr.children, [])
		self.assertEqual(tr.node, tok)
	def test_wrapper(self):
		tok = tokens.Token(tokens.TokenType.LINEBREAK, "!b")
		tr = tree.Tree(tok)
		self.assertTrue(semantics._semantics(tr))
		_assert_flags(self, tr, 'A')
		self.assertEqual(tr.children, [])
		self.assertEqual(tr.node, tok)

class TestLists(unittest.TestCase):
	def test_list(self):
		bl = tree.from_xml('<bl><li/></bl>')
		nl = tree.from_xml('<nl><li/></nl>')
		errs = []
		self.assertTrue(semantics._handle_list(bl, errs))
		self.assertEqual(errs, [])
		_assert_flags(self, bl, 'A')
		_assert_flags(self, bl.children[0], 'A')
		self.assertTrue(semantics._handle_list(nl, errs))
		self.assertEqual(errs, [])
		_assert_flags(self, nl, 'A')
		_assert_flags(self, nl.children[0], 'A')
	def test_floating(self):
		tok = tokens.Token(tokens.TokenType.LIST_ITEM, "!li")
		root = tree.Tree(tree.Root)
		li = tree.Tree(tok)
		root.add(li)
		errs = []
		self.assertFalse(semantics._handle_list_item(li, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_CONTEXT)
		self.assertEqual(errs[0].token, tok)
	def test_content(self):
		tr = tree.from_xml('<bl><group><b/></group></bl>')
		errs = []
		self.assertFalse(semantics._handle_list(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_TOKEN_TYPE)
		self.assertEqual(errs[0].token.type_, tokens.TokenType.LINEBREAK)
	def test_whitespace(self):
		tr = tree.from_xml('<bl><group><li/><text lexeme="\n\t"/><li/></group></bl>')
		text = tr.children[0].children[1]
		errs = []
		self.assertTrue(semantics._handle_list(tr, errs))
		items = tr.children[0].children
		self.assertEqual(len(errs), 0)
		self.assertEqual(len(items), 2)
		self.assertEqual(items[0].node.type_, tokens.TokenType.LIST_ITEM)
		self.assertEqual(items[1].node.type_, tokens.TokenType.LIST_ITEM)
		_assert_flags(self, text, "R")

class TestComment(unittest.TestCase):
	def test_comment_removed(self):
		p = tree.Tree(tree.Root)
		c = tree.Tree(tokens.Token(tokens.TokenType.COMMENT, "!/"))
		p.add(c)
		errs = []
		self.assertTrue(semantics._handle_comment(c, errs))
		self.assertEqual(errs, [])
		self.assertEqual(p.children, [])
		self.assertEqual(c.parent, None)
		_assert_flags(self, c, 'AR')

class TestDelimiter(unittest.TestCase):
	def test_delim_converted(self):
		d = tree.Tree(tokens.Token(tokens.TokenType.DELIMITER, "}"))
		errs = []
		self.assertTrue(semantics._handle_delimiter(d, errs))
		self.assertEqual(errs, [])
		self.assertEqual(d.node.type_, tokens.TokenType.TEXT)
		_assert_flags(self, d, 'A')

class TestImage(unittest.TestCase):
	def test_fully_specified(self):
		tr = tree.from_xml('<image><args><text lexeme="10pt,20pt"/></args>' +
			'<group><text lexeme="https://example.com/example.png"/></group>' +
			'<group><text lexeme="Example image"/></group></image>')
		errs = []
		self.assertTrue(semantics._handle_image(tr, errs))
		self.assertEqual(errs, [])
		args = tr.children[0]
		_assert_flags(self, args, 'A')
		self.assertEqual(len(args.children), 2)
		self.assertEqual(args.children[0].node.lexeme, "10pt")
		self.assertEqual(args.children[1].node.lexeme, "20pt")
		group1 = tr.children[1]
		_assert_flags(self, group1, 'A')
		self.assertEqual(len(group1.children), 1)
		self.assertEqual(group1.children[0].node.lexeme, "https://example.com/example.png")
		group2 = tr.children[2]
		_assert_flags(self, group2, 'A')
		self.assertEqual(len(group2.children), 1)
		self.assertEqual(group2.children[0].node.lexeme, "Example image")
		_assert_flags(self, tr, 'A')
	def test_argc(self):
		tr = tree.from_xml('<image><args><text lexeme="10pt,20pt,5pt"/></args><group/><group/></image>')
		errs = []
		self.assertFalse(semantics._handle_image(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INCORRECT_ITEM_COUNT)
		self.assertEqual(errs[0].token, tokens.Token(tokens.TokenType.TEXT, "5pt"))
	def test_argc_2(self):
		tr = tree.from_xml('<image><args><text lexeme="10pt,"/></args><group/><group/></image>')
		errs = []
		self.assertTrue(semantics._handle_image(tr, errs))
		self.assertEqual(len(errs), 0)
	def test_argv(self):
		tr = tree.from_xml('<image><args><text lexeme="10pt,20xyz"/></args><group/><group/></image>')
		errs = []
		self.assertFalse(semantics._handle_image(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_ARGUMENT)
		self.assertEqual(errs[0].token.lexeme, "20xyz")
	def test_bad_url(self):
		tr = tree.from_xml('<image><args><text lexeme="10pt,20pt"/></args>' +
			'<group><b/></group><group><text lexeme="Example image"/></group></image>')
		errs = []
		self.assertFalse(semantics._handle_image(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_TOKEN_TYPE)
		self.assertEqual(errs[0].token.type_, tokens.TokenType.LINEBREAK)

class TestLink(unittest.TestCase):
	def test_fully_specified(self):
		tr = tree.from_xml('<link><group><text lexeme="https://example.com"/></group>' +
			'<group><text lexeme="example.com"/></group></link>')
		errs = []
		self.assertTrue(semantics._handle_link(tr, errs))
		self.assertEqual(errs, [])
	def test_unnamed(self):
		tr = tree.from_xml('<link><group><text lexeme="https://example.com"/></group><group/></link>')
		errs = []
		self.assertTrue(semantics._handle_link(tr, errs))
		self.assertEqual(errs, [])
	def test_bad_url(self):
		tr = tree.from_xml('<link><group><b/></group><group/></link>')
		errs = []
		self.assertFalse(semantics._handle_link(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_TOKEN_TYPE)
		self.assertEqual(errs[0].token.type_, tokens.TokenType.LINEBREAK)

class TestPosition(unittest.TestCase):
	def test_fully_specified(self):
		tr = tree.from_xml('<position><args><text lexeme="10pt,20pt"/></args>' +
			'<group><b/></group></position>')
		errs = []
		self.assertTrue(semantics._handle_position(tr, errs))
		self.assertEqual(errs, [])
		args = tr.children[0]
		_assert_flags(self, args, 'A')
		self.assertEqual(len(args.children), 2)
		self.assertEqual(args.children[0].node.lexeme, "10pt")
		self.assertEqual(args.children[1].node.lexeme, "20pt")
		content = tr.children[1]
		_assert_flags(self, content, 'A')
		self.assertEqual(len(content.children), 1)
		self.assertEqual(content.children[0].node.type_, tokens.TokenType.LINEBREAK)
	def test_argc(self):
		tr = tree.from_xml('<position><args><text lexeme="10pt"/></args><group><b/></group></position>')
		errs = []
		self.assertFalse(semantics._handle_position(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INCORRECT_ITEM_COUNT)
		self.assertEqual(errs[0].token.lexeme, "10pt")
	def test_argv(self):
		tr = tree.from_xml('<position><args><text lexeme="10pt,xpt"/></args><group><b/></group></position>')
		errs = []
		self.assertFalse(semantics._handle_position(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_ARGUMENT)
		self.assertEqual(errs[0].token.lexeme, "xpt")

class TestText(unittest.TestCase):
	def test_removal(self):
		r1 = tree.Tree(tree.Root)
		r2 = tree.Tree(tree.Root)
		r3 = tree.Tree(tree.Root)
		r4 = tree.Tree(tree.Root)
		r5 = tree.Tree(tree.Root)
		t1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "\tfoo\nbar"))
		t2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo \n\tbar"))
		t3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo\n bar\t"))
		t4 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "\nfoobar"))
		t5 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foobar\n"))
		r1.add(t1)
		r2.add(t2)
		r3.add(t3)
		r4.add(t4)
		r5.add(t5)
		errs = []
		self.assertTrue(semantics._handle_text(t1, errs))
		self.assertTrue(semantics._handle_text(t2, errs))
		self.assertTrue(semantics._handle_text(t3, errs))
		self.assertTrue(semantics._handle_text(t4, errs))
		self.assertTrue(semantics._handle_text(t5, errs))
		self.assertEqual(len(errs), 0)
		self.assertEqual(r1.children[0].node.lexeme, "foo bar")
		self.assertEqual(r2.children[0].node.lexeme, "foo  bar")
		self.assertEqual(r3.children[0].node.lexeme, "foo  bar")
		self.assertEqual(r4.children[0].node.lexeme, "foobar")
		self.assertEqual(r5.children[0].node.lexeme, "foobar")
	def test_par_break(self):
		r1 = tree.Tree(tree.Root)
		r2 = tree.Tree(tree.Root)
		r3 = tree.Tree(tree.Root)
		t1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "\n\nfoobar"))
		t2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo\n\nbar"))
		t3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foobar\n\n"))
		r1.add(t1)
		r2.add(t2)
		r3.add(t3)
		errs = []
		self.assertTrue(semantics._handle_text(t1, errs))
		self.assertTrue(semantics._handle_text(t2, errs))
		self.assertTrue(semantics._handle_text(t3, errs))
		self.assertEqual(len(errs), 0)
		self.assertEqual(r1.children[0].node.type_, tokens.TokenType.PARAGRAPH_BREAK)
		self.assertEqual(r1.children[1].node.lexeme, "foobar")
		self.assertEqual(r2.children[0].node.lexeme, "foo")
		self.assertEqual(r2.children[1].node.type_, tokens.TokenType.PARAGRAPH_BREAK)
		self.assertEqual(r2.children[2].node.lexeme, "bar")
		self.assertEqual(r3.children[0].node.lexeme, "foobar")
		self.assertEqual(r3.children[1].node.type_, tokens.TokenType.PARAGRAPH_BREAK)
	def test_trailing(self):
		r1 = tree.Tree(tree.Root)
		r2 = tree.Tree(tree.Root)
		t1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo\n"))
		t2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo\n"))
		l2 = tree.Tree(tokens.Token(tokens.TokenType.LINK, "!link"))
		r1.add(t1)
		r2.add(t2)
		r2.add(l2)
		errs = []
		self.assertTrue(semantics._handle_text(t1, errs))
		self.assertTrue(semantics._handle_text(t2, errs))
		self.assertEqual(len(errs), 0)
		self.assertEqual(len(r1.children), 1)
		self.assertEqual(r1.children[0].node.lexeme, "foo")
		self.assertEqual(len(r2.children), 2)
		self.assertEqual(r2.children[0].node.lexeme, "foo ")
		self.assertEqual(r2.children[1].node.type_, tokens.TokenType.LINK)

class TestTextPlain(unittest.TestCase):
	def test_replacement(self):
		r1 = tree.Tree(tree.Root)
		r2 = tree.Tree(tree.Root)
		r3 = tree.Tree(tree.Root)
		r1.flags['P'] = True
		r2.flags['P'] = True
		r3.flags['P'] = True
		t1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "\nfoobar"))
		t2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo\nbar"))
		t3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foobar\n"))
		r1.add(t1)
		r2.add(t2)
		r3.add(t3)
		errs = []
		self.assertTrue(semantics._handle_text(t1, errs))
		self.assertTrue(semantics._handle_text(t2, errs))
		self.assertTrue(semantics._handle_text(t3, errs))
		self.assertEqual(len(errs), 0)
		self.assertEqual(r1.children[0].node.type_, tokens.TokenType.LINEBREAK)
		self.assertEqual(r1.children[1].node.lexeme, "foobar")
		self.assertEqual(r2.children[0].node.lexeme, "foo")
		self.assertEqual(r2.children[1].node.type_, tokens.TokenType.LINEBREAK)
		self.assertEqual(r2.children[2].node.lexeme, "bar")
		self.assertEqual(r3.children[0].node.lexeme, "foobar")
		self.assertEqual(r3.children[1].node.type_, tokens.TokenType.LINEBREAK)

class TestTable(unittest.TestCase):
	def test_good(self):
		tr = tree.from_xml('<table><args><text lexeme="sidebar,noborder"/></args>' +
			'<group><row><group><text lexeme="cell content"/></group></row></group></table>')
		errs = []
		self.assertTrue(semantics._handle_table(tr, errs))
		self.assertEqual(len(errs), 0)
	def test_empty(self):
		tr = tree.from_xml('<table><args/><group/></table>')
		errs = []
		self.assertTrue(semantics._handle_table(tr, errs))
		self.assertEqual(len(errs), 0)
	def test_bad_content(self):
		tr = tree.from_xml('<table><args/><group><text lexeme="not a table row"/></group></table>')
		errs = []
		self.assertFalse(semantics._handle_table(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_TOKEN_TYPE)
	def test_bad_args(self):
		tr = tree.from_xml('<table><args><text lexeme="argDoesNotExist"/></args><group/></table>')
		errs = []
		self.assertFalse(semantics._handle_table(tr, errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_ARGUMENT)
	def test_floating_row(self):
		tr = tree.from_xml('<root><row><group/></row></root>')
		errs = []
		self.assertFalse(semantics._handle_table_row(tr.children[0], errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_CONTEXT)
	def test_floating_row_2(self):
		tr = tree.from_xml('<root><group><row><group/></row></group></root>')
		errs = []
		self.assertFalse(semantics._handle_table_row(tr.children[0].children[0], errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_CONTEXT)
	def test_floating_row_3(self):
		tr = tree.from_xml('<position><row><group/></row></position>')
		errs = []
		self.assertFalse(semantics._handle_table_row(tr.children[0], errs))
		self.assertEqual(len(errs), 1)
		self.assertEqual(errs[0].error, errors.Semantic.INVALID_CONTEXT)

if __name__ == "__main__":
	unittest.main()
