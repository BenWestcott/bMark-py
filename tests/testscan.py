"""
@file      testscan.py
@copyright Benjamin Westcott, 2022
@brief     Functionality tests for scan.py and tokens.py
"""
import tempfile
import sys
import unittest
sys.path.append('..')
import scan
import tokens

_BLOCKQUOTE = tokens.Token(tokens.TokenType.BLOCKQUOTE, "!blockquote")
_OPENBRACE = tokens.Token(tokens.TokenType.DELIMITER, "{")
_CLOSEBRACE = tokens.Token(tokens.TokenType.DELIMITER, "}")

class TestNotLockedDown(unittest.TestCase):
	def test_printers(self):
		tt = tokens.TokenType.DELIMITER
		str(tt)
		
		t = tokens.Token(tokens.TokenType.LINEBREAK, "!b")
		str(t)
		t.str_msg()
		hash(t)
		
		m = scan._Match(10, scan._CHUNK_MAX-1, tokens.TokenType.TEXT)
		str(m)
		repr(m)
		m.realign_to = 300
		str(m)
		
		with tempfile.TemporaryFile("w+") as f:
			s = scan.Scanner(f, 30, True)
			str(s)
			repr(s)
			s._dbg("foo")
	def test_debug_parts(self):
		# Debug token printer for long tokens
		with tempfile.TemporaryFile("w+") as f:
			f.write('x'*300)
			f.seek(0)
			s = scan.Scanner(f, 200, True)
			for token in s.tokens():
				pass

class TestMatch(unittest.TestCase):
	def test_all(self):
		m = scan._Match(10, scan._CHUNK_MAX-1, tokens.TokenType.TEXT)
		self.assertFalse(m.needs_realign())
		m = scan._Match.realign(300)
		self.assertTrue(m.needs_realign)
		self.assertEqual(m.realign_to, 300)

class TestScanner(unittest.TestCase):
	def test_chunks_overfull(self):
		f = tempfile.TemporaryFile("w+")
		xs = "x" * 30
		ys = "y" * 30
		f.write(xs + ys)
		f.seek(0)
		scn = scan.Scanner(f, 30)
		scn._adv()
		self.assertEqual(scn._chunk, xs)
		scn._adv()
		self.assertEqual(scn._chunk, ys)
		scn._realign(29)
		self.assertEqual(scn._chunk, "y")
		f.close()
	def test_chunks_underfull(self):
		with tempfile.TemporaryFile("w+") as f:
			f.write("foo")
			f.seek(0)
			scn = scan.Scanner(f, 30)
			scn._adv()
			self.assertEqual(scn._chunk, "foo")
	def test_finders(self):
		f = tempfile.TemporaryFile("w+")
		f.write("!blockquote{}"*2)
		f.seek(0)
		scn = scan.Scanner(f, 20)
		scn._adv()
		
		keywords = scn._find_keywords()
		self.assertEqual(len(keywords), 2)
		self.assertEqual(keywords[0].start, 0)
		self.assertEqual(keywords[0].end, 11)
		self.assertEqual(keywords[0].type_, tokens.TokenType.BLOCKQUOTE)
		self.assertFalse(keywords[0].needs_realign())
		self.assertTrue(keywords[1].start > keywords[0].end)
		self.assertTrue(keywords[1].needs_realign())
		self.assertEqual(keywords[1].realign_to, 13)
		delims = scn._find_delimiters()
		self.assertEqual(delims, [11, 12])
		
		scn._realign(13)
		keywords = scn._find_keywords()
		self.assertEqual(len(keywords), 1)
		self.assertEqual(keywords[0].start, 0)
		self.assertEqual(keywords[0].end, 11)
		self.assertEqual(keywords[0].type_, tokens.TokenType.BLOCKQUOTE)
		self.assertFalse(keywords[0].needs_realign())
		delims = scn._find_delimiters()
		self.assertEqual(delims, [11, 12])
		
		toks = scn._find_tokens()
		self.assertEqual(len(toks), 3)
		self.assertEqual(toks[0], scan._Match(0, 11, tokens.TokenType.BLOCKQUOTE))
		self.assertEqual(toks[11], "{")
		self.assertEqual(toks[12], "}")
		
		f.seek(0)
		toks = []
		for token in scn.tokens():
			toks.append(token)
		good = [_BLOCKQUOTE, _OPENBRACE, _CLOSEBRACE] * 2
		self.assertEqual(toks, good)
		f.close()
	def test_pre_token(self):
		with tempfile.TemporaryFile("w+") as f:
			f.write("x"*5 + "!b" + "x"*5)
			f.seek(0)
			s = scan.Scanner(f, 20)
			toks = []
			for token in s.tokens():
				toks.append(token)
			xs = tokens.Token(tokens.TokenType.TEXT, "x"*5)
			good = [ xs, tokens.Token(tokens.TokenType.LINEBREAK, "!b"), xs]
			self.assertEqual(toks, good)
	def test_auto_realign(self):
		with tempfile.TemporaryFile("w+") as f:
			f.write("x"*20 + "!blockquote" + "x"*5 + "!b")
			f.seek(0)
			s = scan.Scanner(f, 25, True)
			toks = []
			for token in s.tokens():
				toks.append(token)
			good = [
				tokens.Token(tokens.TokenType.TEXT, "x"*20),
				_BLOCKQUOTE,
				tokens.Token(tokens.TokenType.TEXT, "x"*5),
				tokens.Token(tokens.TokenType.LINEBREAK, "!b") ]
			print("TOKENS:  ", toks)
			print("EXPECTED:", good)
			self.assertEqual(toks, good)
	def test_input_checking(self):
		bad_obj = "notafile"
		self.assertRaises(ValueError, scan.Scanner, bad_obj)
		
		with tempfile.TemporaryFile("w+b") as bin_file:
			self.assertRaises(ValueError, scan.Scanner, bin_file)
		
		with tempfile.TemporaryFile("w+") as f:
			self.assertRaises(ValueError, scan.Scanner, f, 3)
			self.assertRaises(ValueError, scan.Scanner, f, scan._CHUNK_MAX+1)

class TestBufferedScanner(unittest.TestCase):
	def test_underfull(self):
		f = tempfile.TemporaryFile("w+")
		f.write("foo")
		f.seek(0)
		s = scan.BufferedScanner(f, 2, 3, 20, True)
		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 1)
		self.assertEqual(s.next(), tokens.Token(tokens.TokenType.TEXT, "foo"))
		self.assertFalse(s.has_next())
		self.assertEqual(s.available(), 0)
		f.close()
	def test_overfull(self):
		f = tempfile.TemporaryFile("w+")
		f.write("!blockquote{}"*2)
		f.seek(0)
		s = scan.BufferedScanner(f, 2, 3, 20)

		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 3)
		self.assertEqual(s.peek(), _BLOCKQUOTE)
		self.assertEqual(s.peek(1), _OPENBRACE)
		self.assertEqual(s.peek(2), _CLOSEBRACE)
		
		self.assertEqual(s.next(), _BLOCKQUOTE)
		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 3)
		self.assertEqual(s.peek(-1), _BLOCKQUOTE)
		self.assertEqual(s.peek(), _OPENBRACE)
		self.assertEqual(s.peek(1), _CLOSEBRACE)
		self.assertEqual(s.peek(2), _BLOCKQUOTE)
		
		self.assertEqual(s.next(), _OPENBRACE)
		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 3)
		self.assertEqual(s.peek(-2), _BLOCKQUOTE)
		self.assertEqual(s.peek(-1), _OPENBRACE)
		self.assertEqual(s.peek(), _CLOSEBRACE)
		self.assertEqual(s.peek(1), _BLOCKQUOTE)
		self.assertEqual(s.peek(2), _OPENBRACE)
		
		self.assertEqual(s.next(), _CLOSEBRACE)
		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 3)
		self.assertEqual(s.peek(-3), _BLOCKQUOTE)
		self.assertEqual(s.peek(-2), _OPENBRACE)
		self.assertEqual(s.peek(-1), _CLOSEBRACE)
		self.assertEqual(s.peek(), _BLOCKQUOTE)
		self.assertEqual(s.peek(1), _OPENBRACE)
		self.assertEqual(s.peek(2), _CLOSEBRACE)
		
		self.assertEqual(s.next(), _BLOCKQUOTE)
		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 2)
		self.assertEqual(s.peek(-3), _OPENBRACE)
		self.assertEqual(s.peek(-2), _CLOSEBRACE)
		self.assertEqual(s.peek(-1), _BLOCKQUOTE)
		self.assertEqual(s.peek(), _OPENBRACE)
		self.assertEqual(s.peek(1), _CLOSEBRACE)
		
		self.assertEqual(s.next(), _OPENBRACE)
		self.assertTrue(s.has_next())
		self.assertEqual(s.available(), 1)
		self.assertEqual(s.peek(-3), _CLOSEBRACE)
		self.assertEqual(s.peek(-2), _BLOCKQUOTE)
		self.assertEqual(s.peek(-1), _OPENBRACE)
		self.assertEqual(s.peek(), _CLOSEBRACE)
		
		self.assertEqual(s.next(), _CLOSEBRACE)
		self.assertFalse(s.has_next())
		self.assertEqual(s.available(), 0)
		self.assertEqual(s.peek(-3), _BLOCKQUOTE)
		self.assertEqual(s.peek(-2), _OPENBRACE)
		self.assertEqual(s.peek(-1), _CLOSEBRACE)
		f.close()
	def test_input_checking(self):
		with tempfile.TemporaryFile("w+") as f:
			self.assertRaises(ValueError, scan.BufferedScanner, f, -1)
			self.assertRaises(ValueError, scan.BufferedScanner, f, 1, -1)
	def test_other_errors(self):
		# Out of tokens
		f = tempfile.TemporaryFile("w+")
		s = scan.BufferedScanner(f, 0)
		self.assertRaises(IndexError, s.next)
		# Peek before start of document
		self.assertRaises(IndexError, s.peek, -1)
		# Peek after end of buffer
		self.assertRaises(IndexError, s.peek)
		f.close()

if __name__ == "__main__":
	unittest.main()
