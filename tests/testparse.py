"""
@file      testparse.py
@copyright Benjamin Westcott, 2022
@brief     Functionality tests for Parse.py, parsedata.py, and errors.py
"""
import tempfile
import sys
import unittest
sys.path.append('..')
import parse
import parsedata
import tree
import scan
import errors
import tokens
import util


class CtxMaker:
	def __init__(self, scanContent:str, chunk:int=512, debug:bool=False, continue_:bool=False):
		self.file = tempfile.TemporaryFile("w+")
		self.file.write(scanContent)
		self.file.seek(0)
		self.scan = scan.BufferedScanner(self.file, 1, 1, chunk, debug)
		self.errors = []
		self.tree = tree.Tree(tree.Root)
		self.context = parsedata.ParseCtx(self.scan, self.errors, self.tree, continue_)
	
	def reset(self):
		self.file.seek(0)
		self.scan = scan.BufferedScanner(self.file, 1, 1)
		self.context.scan = self.scan
		self.tree.children = []
		self.tree.visited = False
		self.errors = []
	
	def __del__(self):
		self.file.close()

def _assert_tree_node(test:any, tree:tree.Tree, type_:tokens.TokenType, lexeme:str, children:int):
	test.assertEqual(tree.node.type_, type_)
	if lexeme:
		test.assertEqual(tree.node.lexeme, lexeme)
	test.assertEqual(len(tree.children), children)

def _assert_parse_good(test:any, fn:callable, ctx:CtxMaker) -> None:
	r = fn(ctx.context)
	test.assertEqual(r, True)
	test.assertEqual(len(ctx.errors), 0)
	test.assertEqual(len(ctx.tree.children), 1)

class TestNotLockedDown(unittest.TestCase):
	# Exercise code paths whose exact results don't need to be locked down
	# Effectively a smoke-test for compile and data-type errors
	def test_debug_utils(self):
		ctx = CtxMaker("")
		p = parse.Parser(ctx.scan, debug=True)
		p._dbg("foo")
	def test_parse_tree_strings(self):
		pt = tree.Tree()
		pt.flags['A'] = True
		pt.add(tree.Tree())
		hash(pt)
		str(pt)
		repr(pt)
		pt.str_shallow()
	def test_errors_printers(self):
		str(errors.Parse.UNCLOSED_BRACE)
		str(errors.Semantic.INVALID_ARGUMENT)
		tok = tokens.Token(tokens.TokenType.TEXT, "foo")
		err = errors.CompileError(0, errors.Semantic.INVALID_ARGUMENT, tok, "bar")
		str(err)
		repr(err)

class TestFatalerrors(unittest.TestCase):
	def test_fail_fast(self):
		tok = tokens.Token(tokens.TokenType.TEXT, "foo")
		rst = errors.DEBUG_ERRORS_AS_EXCEPTIONS
		errors.DEBUG_ERRORS_AS_EXCEPTIONS = True
		self.assertRaises(RuntimeError,
			errors.CompileError, 0, errors.Semantic.INVALID_ARGUMENT, tok)
		errors.DEBUG_ERRORS_AS_EXCEPTIONS = rst
	def test_bad_index(self):
		# Construct a tree with broken double-linkage
		pt = tree.Tree()
		pt2 = tree.Tree()
		pt2.parent = pt
		self.assertRaises(RuntimeError, tree.Tree.index, pt2)
	def test_bad_flag_loop(self):
		# Loop by flag without setting said flag
		# Raises an error to prevent accidental infinite loops
		def bad_loop(tr:tree.Tree):
			for subtree in tr.by_flag('A', False):
				pass
		pt = tree.Tree()
		pt2 = tree.Tree()
		pt.add(pt2)
		self.assertRaises(RuntimeError, bad_loop, pt)

class TestTopLevelParser(unittest.TestCase):
	def test_basic(self):
		ctx = CtxMaker("!b")
		p = parse.Parser(ctx.scan)
		r = p.parse()
		self.assertTrue(r)
		self.assertEqual(len(p.errors), 0)
		self.assertEqual(p.tree.node, tree.Root)
		self.assertEqual(len(p.tree.children), 1)
		_assert_tree_node(self, p.tree.children[0], tokens.TokenType.LINEBREAK, "!b", 0)

class TestParseTree(unittest.TestCase):
	def test_add(self):
		# Append
		pt = tree.Tree()
		self.assertEqual(pt.children, [])
		st1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "1"))
		pt.add(st1)
		self.assertEqual(pt.children, [st1])
		self.assertEqual(st1.parent, pt)
		# Insert
		st2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "2"))
		pt.add(st2, 0)
		self.assertEqual(pt.children, [st2, st1])
		self.assertEqual(st2.parent, pt)
		# add_token
		tok = tokens.Token(tokens.TokenType.UNDEFINED, "")
		pt.add_token(tok)
		st3 = tree.Tree(tok)
		st3.parent = pt
		self.assertEqual(pt.children, [st2, st1, st3])
	def test_remove(self):
		pt = tree.Tree()
		st1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "1"))
		st2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "2"))
		st3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "3"))
		st4 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "4"))
		pt.add(st1)
		pt.add(st2)
		pt.add(st3)
		pt.add(st4)
		self.assertEqual(pt.children, [st1, st2, st3, st4])
		self.assertEqual(pt.remove(1), st2)
		self.assertEqual(pt.children, [st1, st3, st4])
	def test_index(self):
		pt = tree.Tree()
		st1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "1"))
		st2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "2"))
		st3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "3"))
		pt.add(st1)
		pt.add(st2)
		pt.add(st3)
		self.assertEqual(st1.index(), 0)
		self.assertEqual(st2.index(), 1)
		self.assertEqual(st3.index(), 2)
	def test_next(self):
		#     root
		#   /  |  \
		# st1 st2 st3
		#     / \
		#   st4 st5
		pt = tree.Tree()
		st1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "1"))
		st2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "2"))
		st3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "3"))
		st4 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "4"))
		st5 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "5"))
		pt.add(st1)
		pt.add(st2)
		pt.add(st3)
		st2.add(st4)
		st2.add(st5)
		self.assertEqual(st1.next(), st2)
		self.assertEqual(st2.next(), st3)
		self.assertEqual(st3.next(), None)
		self.assertEqual(st4.next(), st5)
		self.assertEqual(st5.next(), st3)
	def test_unvisited1(self):
		pt = tree.Tree()
		pt1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "3"))
		pt.add(pt1)
		unvisited = []
		for uv in pt.by_flag('A', False):
			uv.flags['A'] = True
			unvisited.append(uv)
			self.assertLessEqual(len(unvisited), 1)
		self.assertEqual(len(unvisited), 1)
		self.assertEqual(unvisited[0], pt1)
		
		pt1.flags['A'] =  True
		unvisited = []
		for uv in pt.by_flag('A', False):
			uv.flags['A'] = True
			unvisited.append(uv)
			self.assertEqual(len(unvisited), 0)
	def test_unvisited5(self):
		pt = tree.Tree()
		pt1 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "1"))
		pt2 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "2"))
		pt3 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "3"))
		pt4 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "4"))
		pt5 = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "5"))
		pt.add(pt1)
		pt.add(pt2)
		pt.add(pt3)
		pt.add(pt4)
		pt.add(pt5)
		pt1.flags['A'] = True
		pt4.flags['A'] = True
		pt5.flags['A'] = True
		unvisited = []
		for uv in pt.by_flag('A', False):
			uv.flags['A'] = True
			unvisited.append(uv)
			self.assertLessEqual(len(unvisited), 5)
		self.assertEqual(len(unvisited), 2)
		self.assertFalse(pt1 in unvisited)
		self.assertTrue(pt2 in unvisited)
		self.assertTrue(pt3 in unvisited)
		self.assertFalse(pt4 in unvisited)
		self.assertFalse(pt5 in unvisited)
	def test_from_xml_trivial(self):
		pt = tree.from_xml('<text/>')
		self.assertEqual(pt.parent, None)
		self.assertEqual(pt.node.type_, tokens.TokenType.TEXT)
		self.assertEqual(pt.node.lexeme, "")
		self.assertEqual(pt.node.pos.offset, -1)
		self.assertEqual(pt.node.pos.lines, [])
		self.assertEqual(pt.str_flags(), "")
		self.assertEqual(len(pt.children), 0)
	def test_from_xml_full(self):
		lines = [10, 20, 30]
		pt = tree.from_xml("""
			<newlines offsets="10;20;30">
				<root>
					<b offset="0"/>
					<blockquote lexeme="!blockquote" offset="5" flags="A">
						<text lexeme="foo"/>
						<link flags="AOE"/>
					</blockquote>
					<comment lexeme="!/" offset="20"/>
				</root>
			</newlines>""")
		
		self.assertEqual(pt.parent, None)
		self.assertEqual(pt.node, tree.Root)
		self.assertEqual(len(pt.children), 3)
		b = pt.children[0]
		self.assertEqual(b.parent, pt)
		self.assertEqual(b.node.type_, tokens.TokenType.LINEBREAK)
		self.assertEqual(b.node.lexeme, "")
		self.assertEqual(b.node.pos.offset, 0)
		self.assertEqual(b.node.pos.lines, lines)
		self.assertEqual(b.str_flags(), "")
		self.assertEqual(len(b.children), 0)
		blockquote = pt.children[1]
		self.assertEqual(blockquote.parent, pt)
		self.assertEqual(blockquote.node.type_, tokens.TokenType.BLOCKQUOTE)
		self.assertEqual(blockquote.node.lexeme, "!blockquote")
		self.assertEqual(blockquote.node.pos.offset, 5)
		self.assertEqual(blockquote.node.pos.lines, lines)
		self.assertEqual(blockquote.str_flags(), "A")
		self.assertEqual(len(blockquote.children), 2)
		text = blockquote.children[0]
		self.assertEqual(text.parent, blockquote)
		self.assertEqual(text.node.type_, tokens.TokenType.TEXT)
		self.assertEqual(text.node.lexeme, "foo")
		self.assertEqual(text.node.pos.offset, -1)
		self.assertEqual(text.node.pos.lines, lines)
		self.assertEqual(text.str_flags(), "")
		self.assertEqual(len(text.children), 0)
		link = blockquote.children[1]
		self.assertEqual(link.parent, blockquote)
		self.assertEqual(link.node.type_, tokens.TokenType.LINK)
		self.assertEqual(link.node.lexeme, "")
		self.assertEqual(link.node.pos.offset, -1)
		self.assertEqual(link.node.pos.lines, lines)
		self.assertEqual(link.str_flags(), "AOE")
		self.assertEqual(len(link.children), 0)
		comment = pt.children[2]
		self.assertEqual(comment.parent, pt)
		self.assertEqual(comment.node.type_, tokens.TokenType.COMMENT)
		self.assertEqual(comment.node.lexeme, "!/")
		self.assertEqual(comment.node.pos.offset, 20)
		self.assertEqual(comment.node.pos.lines, lines)
		self.assertEqual(comment.str_flags(), "")
		self.assertEqual(len(comment.children), 0)
	def test_to_xml_trivial(self):
		pt = tree.Tree(tokens.Token(tokens.TokenType.TEXT, ""))
		xml = pt.to_xml()
		self.assertEqual(xml, b'<text />')
	def test_to_xml_full(self):
		newlines = [10, 20, 30]
		root = tree.Tree(tree.Root)
		b = tree.Tree(tokens.Token(tokens.TokenType.LINEBREAK, "", tokens.Position(0, newlines)))
		root.add(b)
		blockquote = tree.Tree(tokens.Token(tokens.TokenType.BLOCKQUOTE, "!blockquote", tokens.Position(5, newlines)))
		blockquote.flags['A'] = True
		root.add(blockquote)
		text = tree.Tree(tokens.Token(tokens.TokenType.TEXT, "foo"))
		blockquote.add(text)
		link = tree.Tree(tokens.Token(tokens.TokenType.LINK, ""))
		link.flags['A'] = True
		link.flags['O'] = True
		link.flags['E'] = True
		blockquote.add(link)
		comment = tree.Tree(tokens.Token(tokens.TokenType.COMMENT, "!/", tokens.Position(20, newlines)))
		root.add(comment)
		xml = root.to_xml(newlines)
		self.assertEqual(xml, b'<newlines offsets="10;20;30"><root><b offset="0" /><blockquote flags="A" lexeme="!blockquote" offset="5"><text lexeme="foo" /><link flags="AOE" /></blockquote><comment lexeme="!/" offset="20" /></root></newlines>')

class TestParseCtx(unittest.TestCase):
	def test_child(self):
		pt = tree.Tree(tree.Root)
		errs = [1, "foo"]
		c = parsedata.ParseCtx(None, errs, pt, False)
		# make child
		pt2 = tree.Tree("bar")
		cpy = c.child(pt2)
		# ensure relevant values share references
		self.assertEqual(c.scan, cpy.scan)
		self.assertEqual(c.errors, cpy.errors)
		errs.append(3)
		self.assertEqual(cpy.errors, errs)
		self.assertEqual(cpy.parent, pt2)
		self.assertEqual(cpy.continue_, c.continue_)

class TestParseDataParser(unittest.TestCase):
	# Only need to test the failure mode, other code is handled by other tests
	def test_bad(self):
		ctx = CtxMaker("")
		self.assertFalse(parsedata.parse(ctx.context))

class TestExpectDelim(unittest.TestCase):
	def test_good(self):
		ctx = CtxMaker("}!bold")
		self.assertEqual(parsedata._expect_delim(ctx.context, "}"),
			tokens.Token(tokens.TokenType.DELIMITER, "}"))
		self.assertEqual(ctx.scan.peek().type_, tokens.TokenType.BOLD)
	def test_bad(self):
		ctx = CtxMaker("}!bold")
		self.assertIsNone(parsedata._expect_delim(ctx.context, "{"))
		self.assertEqual(ctx.scan.peek().type_, tokens.TokenType.DELIMITER)

def _parse_input_helper_good(test:any, ctx:CtxMaker, isArgs:bool, optional:bool, hasChild:bool):
		test.assertTrue(parsedata._parse_input(ctx.context, isArgs, optional, -100))
		test.assertEqual(len(ctx.tree.children), 1)
		test.assertEqual(len(ctx.errors), 0)
		collection = ctx.tree.children[0]
		ctype = tokens.TokenType.ARGUMENTS if isArgs else tokens.TokenType.GROUP
		if hasChild:
			_assert_tree_node(test, collection, ctype, None, 1)
			_assert_tree_node(test, collection.children[0], tokens.TokenType.TEXT, "foo", 0)
		else:
			_assert_tree_node(test, collection, ctype, None, 0)
def _parse_input_helper_bad(test:any, ctx:CtxMaker, isArgs:bool, optional:bool, hasToken:bool):
		test.assertFalse(parsedata._parse_input(ctx.context, isArgs, optional, -100))
		test.assertEqual(len(ctx.tree.children), 1)
		test.assertEqual(len(ctx.errors), 1)
		ctype = tokens.TokenType.ARGUMENTS if isArgs else tokens.TokenType.GROUP
		_assert_tree_node(test, ctx.tree.children[0], ctype, None, 0)
		err = ctx.errors[0]
		etype = errors.Parse.EXPECTED_PAREN if isArgs else errors.Parse.EXPECTED_BRACE
		test.assertEqual(err.error, etype)
		if hasToken:
			test.assertEqual(err.token, tokens.Token(tokens.TokenType.TEXT, "foo"))
		else:
			test.assertEqual(err.token, tokens.Token(tokens.TokenType.UNDEFINED, ""))
def _parse_input_helper_next(test:any, ctx:CtxMaker, has_next:bool):
	if has_next:
		test.assertTrue(ctx.scan.has_next())
		test.assertEqual(ctx.scan.next(), tokens.Token(tokens.TokenType.TEXT, "foo"))
	else:
		test.assertFalse(ctx.scan.has_next())

class TestParseInput(unittest.TestCase):
	def test_simple_content(self):
		ctx = CtxMaker("{foo}")
		_parse_input_helper_good(self, ctx, False, False, True) # Content required
		_parse_input_helper_next(self, ctx, False)
		ctx.reset()
		_parse_input_helper_good(self, ctx, False, True, True) # Content optional
		_parse_input_helper_next(self, ctx, False)
	def test_simple_args(self):
		ctx = CtxMaker("(foo)")
		_parse_input_helper_good(self, ctx, True, False, True) # Args required
		_parse_input_helper_next(self, ctx, False)
		ctx.reset()
		_parse_input_helper_good(self, ctx, True, True, True) # Args optional
		_parse_input_helper_next(self, ctx, False)
	def test_optional_content(self):
		ctx = CtxMaker("foo")
		_parse_input_helper_bad(self, ctx, False, False, True) # Content required
		_parse_input_helper_next(self, ctx, True)
		ctx.reset()
		_parse_input_helper_good(self, ctx, False, True, False) # Content optional
		_parse_input_helper_next(self, ctx, True)
	def test_optional_args(self):
		ctx = CtxMaker("foo")
		_parse_input_helper_bad(self, ctx, True, False, True) # Args required
		_parse_input_helper_next(self, ctx, True)
		ctx.reset()
		_parse_input_helper_good(self, ctx, True, True, False) # Args optional
		_parse_input_helper_next(self, ctx, True)
	def test_no_content(self):
		ctx =  CtxMaker("")
		_parse_input_helper_bad(self, ctx, False, False, False) # Content required
		ctx.reset()
		_parse_input_helper_good(self, ctx, False, True, False) # Content optional
	def test_no_args(self):
		ctx = CtxMaker("")
		_parse_input_helper_bad(self, ctx, True, False, False) # Args required
		ctx.reset()
		_parse_input_helper_good(self, ctx, True, True, False) # Args optional
	def test_child_failure(self):
		ctx = CtxMaker("{!bold{")
		self.assertFalse(parsedata._parse_input(ctx.context, False, False, -100))
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)

class TestParseStandalone(unittest.TestCase):
	def test_standalone(self):
		ctx = CtxMaker("!b{")
		_assert_parse_good(self, parsedata._parse_standalone, ctx)
		ch = ctx.tree.children[0]
		self.assertEqual(ch.node.type_, tokens.TokenType.LINEBREAK)
		self.assertTrue(ctx.scan.has_next())
		self.assertEqual(ctx.scan.next(), tokens.Token(tokens.TokenType.DELIMITER, "{"))

class testParseSingleBlock(unittest.TestCase):
	def test_good(self):
		# Inline version
		ctx = CtxMaker("!bold{foo!bbar}")
		_assert_parse_good(self, parsedata._parse_single_block, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.BOLD, None, 1)
		group = ch.children[0]
		_assert_tree_node(self, group, tokens.TokenType.GROUP, None, 3)
		_assert_tree_node(self, group.children[0], tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, group.children[1], tokens.TokenType.LINEBREAK, None, 0)
		_assert_tree_node(self, group.children[2], tokens.TokenType.TEXT, "bar", 0)
		# Out-of-line version
		ctx = CtxMaker("!bold{foo!bbar}")
		_assert_parse_good(self, parsedata._parse_single_block_ool, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.BOLD, None, 1)
		self.assertTrue(ch.flags['O'])
		group = ch.children[0]
		_assert_tree_node(self, group, tokens.TokenType.GROUP, None, 3)
		_assert_tree_node(self, group.children[0], tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, group.children[1], tokens.TokenType.LINEBREAK, None, 0)
		_assert_tree_node(self, group.children[2], tokens.TokenType.TEXT, "bar", 0)
	def test_no_content(self):
		# Inline version
		ctx = CtxMaker("!boldfoo")
		r = parsedata._parse_single_block(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)
		# Out-of-line version
		ctx = CtxMaker("!boldfoo")
		r = parsedata._parse_single_block_ool(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)
	def test_unclosed(self):
		# Inline version
		ctx = CtxMaker("!bold{foo")
		r = parsedata._parse_single_block(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)
		# Out-of-line version
		ctx = CtxMaker("!bold{foo")
		r = parsedata._parse_single_block(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)

class TestParseBlockquote(unittest.TestCase):
	def test_plain(self):
		ctx = CtxMaker("!blockquote{foo}")
		_assert_parse_good(self, parsedata._parse_blockquote, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.BLOCKQUOTE, None, 2)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 1)
		ch2 = ch.children[0].children[0]
		_assert_tree_node(self, ch2, tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 0)
	def test_author(self):
		ctx = CtxMaker("!blockquote{foo}{bar}")
		_assert_parse_good(self, parsedata._parse_blockquote, ctx)
		blockquote = ctx.tree.children[0]
		_assert_tree_node(self, blockquote, tokens.TokenType.BLOCKQUOTE, None, 2)
		content = blockquote.children[0]
		_assert_tree_node(self, content, tokens.TokenType.GROUP, None, 1)
		contentch = content.children[0]
		_assert_tree_node(self, contentch, tokens.TokenType.TEXT, "foo", 0)
		author = blockquote.children[1]
		_assert_tree_node(self, author, tokens.TokenType.GROUP, None, 1)
		authorch = author.children[0]
		_assert_tree_node(self, authorch, tokens.TokenType.TEXT, "bar", 0)
	def test_bad(self):
		# No content
		ctx = CtxMaker("!blockquotefoo")
		r = parsedata._parse_blockquote(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)
		# Broken author
		ctx = CtxMaker("!blockquote{foo}{bar")
		r = parsedata._parse_blockquote(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)

class TestParseList(unittest.TestCase):
	def test_empty(self):
		ctx = CtxMaker("!bl{}")
		_assert_parse_good(self, parsedata._parse_list, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.BULLET_LIST, None, 1)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 0)
	def test_items(self):
		ctx = CtxMaker("!nl{!li{foo}!li{bar}!li{baz}}")
		_assert_parse_good(self, parsedata._parse_list, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.NUMBER_LIST, None, 1)
		group = ch.children[0]
		_assert_tree_node(self, group, tokens.TokenType.GROUP, None, 3)
		li1 = group.children[0]
		_assert_tree_node(self, li1, tokens.TokenType.LIST_ITEM, None, 1)
		_assert_tree_node(self, li1.children[0], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, li1.children[0].children[0], tokens.TokenType.TEXT, "foo", 0)
		li2 = group.children[1]
		_assert_tree_node(self, li2, tokens.TokenType.LIST_ITEM, None, 1)
		_assert_tree_node(self, li2.children[0], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, li2.children[0].children[0], tokens.TokenType.TEXT, "bar", 0)
		li3 = group.children[2]
		_assert_tree_node(self, li3, tokens.TokenType.LIST_ITEM, None, 1)
		_assert_tree_node(self, li3.children[0], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, li3.children[0].children[0], tokens.TokenType.TEXT, "baz", 0)
	def test_bad(self):
		ctx = CtxMaker("!blfoo")
		r = parsedata._parse_list(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)

class TestParseCode(unittest.TestCase):
	def test_code(self):
		ctx = CtxMaker("!code{foo(!bar){\!baz;}\}")
		_assert_parse_good(self, parsedata._parse_code, ctx)
		ch = ctx.tree.children[0]
		self.assertFalse(ch.flags['O'])
		_assert_tree_node(self, ch, tokens.TokenType.CODE, None, 1)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 3)
		text = ch.children[0].children
		_assert_tree_node(self, text[0], tokens.TokenType.TEXT, "foo(!bar){", 0)
		_assert_tree_node(self, text[1], tokens.TokenType.LINEBREAK, "!b", 0)
		_assert_tree_node(self, text[2], tokens.TokenType.TEXT, "az;}", 0)
	def test_codeblock(self):
		ctx = CtxMaker("!codeblock{foo(!bar){\!baz;}\}")
		_assert_parse_good(self, parsedata._parse_code, ctx)
		ch = ctx.tree.children[0]
		self.assertTrue(ch.flags['O'])
		_assert_tree_node(self, ch, tokens.TokenType.CODEBLOCK, None, 1)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 3)
		text = ch.children[0].children
		_assert_tree_node(self, text[0], tokens.TokenType.TEXT, "foo(!bar){", 0)
		_assert_tree_node(self, text[1], tokens.TokenType.LINEBREAK, "!b", 0)
		_assert_tree_node(self, text[2], tokens.TokenType.TEXT, "az;}", 0)
	def test_bad(self):
		ctx = CtxMaker("!codefoo")
		r = parsedata._parse_code(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)

class TestParseImage(unittest.TestCase):
	def test_minimal(self):
		ctx = CtxMaker("!image{https://www.example.com/image.png}")
		_assert_parse_good(self, parsedata._parse_image, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.IMAGE, None, 3)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.ARGUMENTS, None, 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 1)
		url = ch.children[1].children[0]
		_assert_tree_node(self, url, tokens.TokenType.TEXT, "https://www.example.com/image.png", 0)
		_assert_tree_node(self, ch.children[2], tokens.TokenType.GROUP, None, 0)
	def test_with_args(self):
		ctx = CtxMaker("!image(0.4, 0.1){banner.svg}")
		_assert_parse_good(self, parsedata._parse_image, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.IMAGE, None, 3)
		args = ch.children[0]
		_assert_tree_node(self, args, tokens.TokenType.ARGUMENTS, None, 1)
		_assert_tree_node(self, args.children[0], tokens.TokenType.TEXT, "0.4, 0.1", 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[1].children[0], tokens.TokenType.TEXT, "banner.svg", 0)
	def test_with_caption(self):
		ctx = CtxMaker("!image{img}{Hello, world!}")
		_assert_parse_good(self, parsedata._parse_image, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.IMAGE, None, 3)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.ARGUMENTS, None, 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[1].children[0], tokens.TokenType.TEXT, "img", 0)
		_assert_tree_node(self, ch.children[2], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[2].children[0], tokens.TokenType.TEXT, "Hello, world!", 0)
	def test_with_args_and_caption(self):
		ctx = CtxMaker("!image(0.2, 0.3){img}{caption}")
		_assert_parse_good(self, parsedata._parse_image, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.IMAGE, None, 3)
		args = ch.children[0]
		_assert_tree_node(self, args, tokens.TokenType.ARGUMENTS, None, 1)
		_assert_tree_node(self, args.children[0], tokens.TokenType.TEXT, "0.2, 0.3", 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[1].children[0], tokens.TokenType.TEXT, "img", 0)
		_assert_tree_node(self, ch.children[2], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[2].children[0], tokens.TokenType.TEXT, "caption", 0)
	def test_bad_args(self):
		ctx = CtxMaker("!image(")
		r = parsedata._parse_image(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_PAREN)
	def test_bad_url(self):
		ctx = CtxMaker("!image(0.2, 0.3)foo")
		r = parsedata._parse_image(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)
	def test_bad_caption(self):
		ctx = CtxMaker("!image{img}{caption")
		r = parsedata._parse_image(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)

class TestParseLink(unittest.TestCase):
	def test_simple(self):
		ctx = CtxMaker("!link{https://example.com}")
		_assert_parse_good(self, parsedata._parse_link, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.LINK, None, 2)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[0].children[0], tokens.TokenType.TEXT, "https://example.com", 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 0)
	def test_named(self):
		ctx = CtxMaker("!link{example.com}{foo!bbar}")
		_assert_parse_good(self, parsedata._parse_link, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.LINK, None, 2)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[0].children[0], tokens.TokenType.TEXT, "example.com", 0)
		group2 = ch.children[1]
		_assert_tree_node(self, group2, tokens.TokenType.GROUP, None, 3)
		_assert_tree_node(self, group2.children[0], tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, group2.children[1], tokens.TokenType.LINEBREAK, None, 0)
		_assert_tree_node(self, group2.children[2], tokens.TokenType.TEXT, "bar", 0)
	def test_bad_url(self):
		ctx = CtxMaker("!linkfoo")
		r = parsedata._parse_link(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)
	def test_bad_name(self):
		ctx = CtxMaker("!link{foo}{bar")
		r = parsedata._parse_link(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)

class TestParsePosition(unittest.TestCase):
	def test_good(self):
		ctx = CtxMaker("!position(0.25, 0.75){foo!bbar}")
		_assert_parse_good(self, parsedata._parse_position, ctx)
		ch = ctx.tree.children[0]

		_assert_tree_node(self, ch, tokens.TokenType.POSITION, None, 2)
		args = ch.children[0]
		_assert_tree_node(self, args, tokens.TokenType.ARGUMENTS, None, 1)
		_assert_tree_node(self, args.children[0], tokens.TokenType.TEXT, "0.25, 0.75", 0)
		content = ch.children[1]
		_assert_tree_node(self, content, tokens.TokenType.GROUP, None, 3)
		_assert_tree_node(self, content.children[0], tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, content.children[1], tokens.TokenType.LINEBREAK, None, 0)
		_assert_tree_node(self, content.children[2], tokens.TokenType.TEXT, "bar", 0)
	def test_badPos(self):
		ctx = CtxMaker("!position{foo}")
		r = parsedata._parse_position(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_PAREN)
	def test_badContent(self):
		ctx = CtxMaker("!position(0.25, 0.75)foo")
		r = parsedata._parse_position(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)

class TestParseTable(unittest.TestCase):
	def test_basic(self):
		ctx = CtxMaker("!table{!row{foo}}")
		_assert_parse_good(self, parsedata._parse_table, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.TABLE, None, 2)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.ARGUMENTS, None, 0)
		group = ch.children[1]
		_assert_tree_node(self, group, tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, group.children[0], tokens.TokenType.TABLE_ROW, None, 1)
	def test_args(self):
		ctx = CtxMaker("!table(wide){!row}")
		_assert_parse_good(self, parsedata._parse_table, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.TABLE, None, 2)
		args = ch.children[0]
		_assert_tree_node(self, args, tokens.TokenType.ARGUMENTS, None, 1)
		_assert_tree_node(self, args.children[0], tokens.TokenType.TEXT, "wide", 0)
		group = ch.children[1]
		_assert_tree_node(self, group, tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, group.children[0], tokens.TokenType.TABLE_ROW, None, 0)
	def test_badArgs(self):
		ctx = CtxMaker("!table(")
		r = parsedata._parse_table(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_PAREN)
	def test_badRows(self):
		ctx = CtxMaker("!table(wide)foo")
		r = parsedata._parse_table(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.EXPECTED_BRACE)

class TestParseTableRow(unittest.TestCase):
	def test_empty(self):
		ctx = CtxMaker("!row")
		_assert_parse_good(self, parsedata._parse_table_row, ctx)
		_assert_tree_node(self, ctx.tree.children[0], tokens.TokenType.TABLE_ROW, None, 0)
	def test_items(self):
		ctx = CtxMaker("!row{foo}{bar}{baz}")
		_assert_parse_good(self, parsedata._parse_table_row, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.TABLE_ROW, None, 3)
		_assert_tree_node(self, ch.children[0], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[0].children[0], tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, ch.children[1], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[1].children[0], tokens.TokenType.TEXT, "bar", 0)
		_assert_tree_node(self, ch.children[2], tokens.TokenType.GROUP, None, 1)
		_assert_tree_node(self, ch.children[2].children[0], tokens.TokenType.TEXT, "baz", 0)
	def test_content(self):
		ctx = CtxMaker("!row{foo!bbar}")
		_assert_parse_good(self, parsedata._parse_table_row, ctx)
		ch = ctx.tree.children[0]
		_assert_tree_node(self, ch, tokens.TokenType.TABLE_ROW, None, 1)
		group = ch.children[0]
		_assert_tree_node(self, group, tokens.TokenType.GROUP, None, 3)
		_assert_tree_node(self, group.children[0], tokens.TokenType.TEXT, "foo", 0)
		_assert_tree_node(self, group.children[1], tokens.TokenType.LINEBREAK, None, 0)
		_assert_tree_node(self, group.children[2], tokens.TokenType.TEXT, "bar", 0)
	def test_bad(self):
		ctx = CtxMaker("!row{")
		r = parsedata._parse_table_row(ctx.context)
		self.assertFalse(r)
		self.assertEqual(len(ctx.errors), 1)
		self.assertEqual(ctx.errors[0].error, errors.Parse.UNCLOSED_BRACE)

class TestParseText(unittest.TestCase):
	def test_simple(self):
		ctx = CtxMaker("foo, bar: baz")
		_assert_parse_good(self, parsedata._parse_text, ctx)
		_assert_tree_node(self, ctx.tree.children[0], tokens.TokenType.TEXT, "foo, bar: baz", 0)
	def test_mergeText(self):
		spaces = tokens.MAX_LEN - 3
		blk1 = "foo" + " "*spaces
		blk2 = "bar" + " "*spaces
		blk3 = "baz"
		txt = blk1 + blk2 + blk3
		ctx = CtxMaker(txt, tokens.MAX_LEN)
		self.assertTrue(ctx.scan.has_next())
		self.assertEqual(ctx.scan.next().lexeme, blk1)
		self.assertTrue(ctx.scan.has_next())
		self.assertEqual(ctx.scan.next().lexeme, blk2)
		self.assertTrue(ctx.scan.has_next())
		self.assertEqual(ctx.scan.next().lexeme, blk3)
		self.assertFalse(ctx.scan.has_next())
		ctx.reset()
		_assert_parse_good(self, parsedata._parse_text, ctx)
		_assert_tree_node(self, ctx.tree.children[0], tokens.TokenType.TEXT, txt, 0)

if __name__ == "__main__":
	unittest.main()
