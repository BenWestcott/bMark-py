"""
@file      tokens.py
@copyright Benjamin Westcott, 2022
@brief     Token types, expressions, and parameters
"""
import re

# TODO: temporary, remove in future refactor
# Indices into the types list. This is fragile and only to facilitate the change to a type config
CODEBLOCK = 12
BULLET_LIST = 8
NUMBER_LIST = 21
LIST_ITEM = 20
LINEBREAK = 10
TABLE_ROW = 24
TABLE_ROW_HEADER = 23
TABLE = 28

"""Matches if the string is entirely whitespace."""
WHITESPACE = re.compile(r"\s*\Z")

class TokenType:
	def __init__(self, regex:re.Pattern, name:str, pretty:str):
		self.regex = regex
		assert type(name) == str
		self.name = name
		self.pretty = pretty
		self.parse_ops = []
		self.semantic_ops = []
	def __str__(self):
		return self.pretty
	def __repr__(self):
		return f"TokenType({repr(self.regex)}, {repr(self.name)}, {repr(self.pretty)})"

class Position:
	def __init__(self, offset, lines_ref:list):
		self.offset = offset
		self.lines = lines_ref
	def coords(self) -> tuple:
		if self.offset == -1:
			return (-1, -1)
		last_newline_idx = -1
		for i in range(len(self.lines)-1, -1, -1):
			if self.lines[i] < self.offset:
				last_newline_idx = i
				break
		if last_newline_idx == -1:
			return (0, self.offset)
		line = last_newline_idx + 2
		col = self.offset - self.lines[last_newline_idx] - 1
		return (line, col)

"""Instance of a token with a lexeme and file position."""
class Token:
	def __init__(self, type_:TokenType, lexeme:str, position:Position=None):
		assert type(type_) == TokenType
		self.type_ = type_
		self.lexeme = lexeme
		self.pos = position
		if position is None:
			self.pos = Position(-1, [])
	
	"""Pretty-print the token for use in user-facing messages."""
	def str_msg(self) -> str:
		lexeme = repr(self.lexeme) if self.lexeme != "" else str(self.type)
		coords = self.pos.coords()
		return f"{lexeme} at line {coords[0]}, col {coords[1]}"
	
	def __hash__(self) -> int:
		return hash((self.type_, self.lexeme, self.pos.offset))
	"""Note that this does not compare position."""
	def __eq__(self, other) -> bool:
		return type(other) == Token and self.type_ == other.type_ and self.lexeme == other.lexeme
	def __str__(self) -> str:
		coords = self.pos.coords()
		return f"{self.type_.name}({self.lexeme!r}@{coords[0]}:{coords[1]})"
	def __repr__(self) -> str:
		return f"Token({self.type_.name!r}, {self.lexeme!r})"
