# BMark
## What is it?
BMark is a simple document markup language designed to be quick to write, easy to read and parse, and reasonably powerful. With regard to ease of writing and and flexibility, it is designed to sit between markdown and LaTeX.

# Status
The BMark compiler has extensive test coverage and no known bugs. The source document format should be relatively stable, though breaking changes may occur without warning. Internal interfaces can and will experience breaking changes without warning. HTML generation is functional but currently only has a testing-oriented interface that is not friendly for conventional use.

Previous prototype versions of the language and compiler exist under the name "SDC" and were implemented in Perl and C. These versions are reasonably complete, but unmaintained and likely buggy as they were not built in a flexible or extensible way. SDC source may be removed from public access without notice. Use of the programs or source as anything more than a curiosity is strongly discouraged.

## Milestones
### 2022
- August 29: Language configuration XML introduced for tokens
- August 13: Full unit-test coverage for semantic analyzer completed and passing
- July 31: Full unit-test coverage for scanner completed and passing
- July 24: End-to-end document generation testing completed without issues
- June 18: Initial implementation of HTML emitter completed
- May 29: Error messages are now useful
- May 27: Initial semantic analyzer implementation and integration with parser completed
- May 19: Full unit-test coverage for parser completed and passing
- May 11: Initial implementations of scanner and parser completed
- May 1: Project started

## Goals
### Short-Term
- Finish unit-tests for completed components
- Improve token position tracking
### Long-Term
- Introduce keywords for managing page layout
- Permit bangs to be escaped
- Make scanner compatible with piped input
- LaTeX codegen
### Moonshot
- PDF codegen
- Table rendering algorithm
- Advanced text layout: justification aglorithm, paragraph scaling for page fit

# Language
BMark documents consist of text and commands. Commands start with a bang, followed by a keyword, optionally followed by arguments and/or content groups, depending on the command type. Whitespace between the individual parts of a command is not allowed. Anything that is not part of a command is considered text.

Arguments are encapsulated by parentheses and separated by commas and, optionally, whitespace. Arguments must not contain any keywords. Arguments providing dimensions must be unitless (fratction of the parent's respective dimension), in percent (of the parent's respective dimension), or in points (pt, the only absolute unit).

Content groups are encapsulated by curly braces and can be filled with text, and possibly additional BMark commands. The ability to include nested commands and the type of nested commands permitted may be restricted by the parent command. For example, a table's content group only accepts table row commands.

## Quirks
For the purposes of making the compiler simple and the language easy to ingest, there are a number of restrictions on the language. Some of these may already be mentioned above.
- All keywords must start with a bang. A keyword regex that does not start with a bang will never be found by the scanner.
- Source that is not recognized as part of a command is interpreted as document text. If an instruction is invoked incorrectly, BMark source may unintentionally appear in the exported document.
- Spaces between keywords, arguments, and content blocks are not allowed. The '(' or '{' must immediately follow the end of the previous part of the command. If whitespace is present, the later part is syntactically detached from the instruction and interpreted as text.
- All tab characters are stripped from source text to enable formatting in the source without affecting the generated document. This only applies to text and does not affect the above restriction on command syntax.
- Solitary newlines in the source document are stripped to permit hard line wrapping if the author chooses. To replace its semantic meaning, !b inserts single line breaks and a double-newline in the source inserts a paragraph break.
