"""
@file      emit_html.py
@copyright Benjamin Westcott, 2022
@brief     Emits a parse tree to HTML
"""
import model
import tokens
import tree

def emit(config:model.Config, fileHandle:any, tr:tree.Tree, debug:bool=False) -> None:
	if tr.node == tree.Root:
		if debug:
			print("Emitting root node")
		fileHandle.write('<!DOCTYPE html><head>\n')
		fileHandle.write('\t<meta charset="utf-8">\n')
		fileHandle.write('\t<link rel=stylesheet href="../html-themes/default.css"/>\n')
		fileHandle.write('</head><body><div class=main>')
		for child in tr.children:
			emit(config, fileHandle, child)
		fileHandle.write("</div></body>")
	else:
		if debug:
			print("Emitting child node", tr.node.type_)
		if tr.node.type_.name in BASIC_TAGS:
			_emit_basic_tag(config, fileHandle, tr)
		else:
			EMITTERS[tr.node.type_.name](config, fileHandle, tr)

def _emit_basic_tag(config:model.Config, fh:any, tr:tree.Tree) -> None:
	tags = BASIC_TAGS[tr.node.type_.name]
	fh.write(tags[0])
	content = tr.children[0]
	for child in content.children:
		emit(config, fh, child)
	fh.write(tags[1])

def _convert_numeric(config:model.Config, tr:tree.Tree) -> str:
	numpart = config.numpart.match(tr.node.lexeme)
	num = numpart.group()
	unit = tr.node.lexeme[numpart.end():]
	if unit == '':
		unit = '%'
		num = str(float(num)*100)
	return num + unit

def _emit_blockquote(config:model.Config, fh:any, tr:tree.Tree) -> None:
	fh.write("<div class=blockquote>")
	content = tr.children[0]
	for child in content.children:
		emit(config, fh, child)
	if len(tr.children) > 1:
		author = tr.children[1]
		fh.write("<div class=bqauthor>")
		for child in author.children:
			emit(config, fh, child)
		fh.write("</div>")
	fh.write("</div>")

def _emit_image(config:model.Config, fh:any, tr:tree.Tree) -> None:
	args = tr.children[0]
	width = None
	height = None
	if len(args.children) != 0:
		width = _convert_numeric(args.children[0])
		height = _convert_numeric(args.children[1])
	url = tr.children[1]
	caption = tr.children[2]
	fh.write(f'<figure><img style="max-width:{width};max-height:{height}" src="{url}"/>')
	if len(caption.children) != 0:
		fh.write(f'<figcaption>{caption.children[0].node.lexeme}</figcaption>')
	fh.write('</figure>')

def _emit_linebreak(config:model.Config, fh:any, tr:tree.Tree) -> None:
	fh.write("<div class=break></div>")

def _emit_link(config:model.Config, fh:any, tr:tree.Tree) -> None:
	fh.write('<a href="')
	g1 = tr.children[0]
	url = g1.children[0].node.lexeme
	fh.write(url)
	fh.write('">')
	text = url
	g2 = tr.children[1]
	if len(g2.children) != 0:
		text = g2.children[0].node.lexeme
	fh.write(text)
	fh.write("</a>")

def _emit_multicol(config:model.Config, fh:any, tr:tree.Tree) -> None:
	args = tr.children[0].children
	content = tr.children[1].children
	fh.write(f'<div class=multicol style="column-count:{int(args[0].node.lexeme)};">')
	for child in content:
		emit(config, fh, child)
	fh.write("</div>")

def _emit_par_break(config:model.Config, fh:any, tr:tree.Tree) -> None:
	fh.write("<div class=parbreak></div>")

def _emit_position(config:model.Config, fh:any, tr:tree.Tree) -> None:
	x = tr.children[0].children[0].node.lexeme
	y = tr.children[0].children[1].node.lexeme
	fh.write(f'<div class=position style="left:{x};top:{y}">')
	for child in tr.children[1].children:
		emit(config, fh, child)
	fh.write('</div>')

def _emit_table(config:model.Config, fh:any, tree:tree.Tree) -> None:
	contentIdx = 0
	args = []
	if tree.children[0].node.type_ == config.type_arguments:
		contentIdx = 1
		for arg in tree.children[0].children:
			args.append(arg.node.lexeme)
	if len(args) == 0:
		fh.write("<table>")
	else:
		fh.write('<table class="')
		for arg in args:
			fh.write(arg)
			fh.write(" ")
		fh.write('">')
	content = tree.children[contentIdx]
	for row in content.children:
		emit(config, fh, row)
	fh.write("</table>")

def _emit_tableRow(config:model.Config, fh:any, tree:tree.Tree, isHeader:bool=False) -> None:
	tags = TABLE_ROW_TAGS[isHeader]
	fh.write(tags[0])
	for group in tree.children:
		fh.write("<td>")
		for child in group.children:
			emit(config, fh, child)
		fh.write("</td>")
	fh.write(tags[1])

def _emit_tableRowHeader(config:model.Config, fh:any, tr:tree.Tree) -> None:
	_emit_tableRow(config, fh, tr, True)

def _emit_text(config:model.Config, fh:any, tr:tree.Tree) -> None:
	escaped = tr.node.lexeme.replace("&", "&amp;")
	escaped = escaped.replace("<", "&lt;")
	escaped = escaped.replace(">", "&gt;")
	fh.write(escaped)


TABLE_ROW_TAGS = [
	["<tr>", "</tr>"],
	["<tr class=header>", "</tr>"]
]

BASIC_TAGS = {
	"bold": ["<b>", "</b>"],
	"bl": ["<ul>", "</ul>"],
	"center": ["<center>", "</center>"],
	"code": ["<span class=code>", "</span>"],
	"codeblock": ["<div class=code>", "</div>"],
	"h1": ["<h2>", "</h2>"],
	"h2": ["<h3>", "</h3>"],
	"h3": ["<h4>",  "</h4>"],
	"italic": ["<span class=ital>", "</span>"],
	"li": ["<li>", "</li>"],
	"nl": ["<ol>", "</ol>"],
	"s": ["<span class=slant>", "</span>"],
	"strike": ["<span class=strike>", "</span>"],
	"subtitle": ["<h1 class=subtitle>", "</h1>"],
	"t": ["<span class=tab>", "</span>"],
	"title": ["<h1>", "</h1>"],
	"under": ["<span class=under>", "</span>"]
}

EMITTERS = {
	"blockquote": _emit_blockquote,
	"image": _emit_image,
	"b": _emit_linebreak,
	"link": _emit_link,
	"multicol": _emit_multicol,
	"pb": _emit_par_break,
	"position": _emit_position,
	"table": _emit_table,
	"row": _emit_tableRow,
	"rowheader": _emit_tableRowHeader,
	"text": _emit_text
}
