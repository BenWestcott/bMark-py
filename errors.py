"""
@file      errors.py
@copyright Benjamin Westcott, 2022
@brief     Passive data structures for representing error conditions
"""
from enum import Enum, auto
import tokens

# When true, raise an exception when constructing an error data object
DEBUG_ERRORS_AS_EXCEPTIONS = True

"""Types of parse errors."""
class Parse(Enum):
	GENERIC = auto()
	INVALID_CONTEXT = auto()
	INVALID_TOKEN_TYPE = auto()
	UNEXPECTED_TOKEN = auto()
	MISSING_DELIM = auto()
	MISSING_ARGUMENT = auto()
	EXPECTED_BRACE = auto()
	UNCLOSED_BRACE = auto()
	EXPECTED_PAREN = auto()
	UNCLOSED_PAREN = auto()
	"""Human message format. For a compact format, use repr."""
	def __str__(self) -> str:
		return PARSE_ENUM_STRS[self]

"""Pretty human strings for parse error types."""
PARSE_ENUM_STRS = {
	Parse.GENERIC: "<unknown type>",
	Parse.INVALID_CONTEXT: "invalid token context",
	Parse.INVALID_TOKEN_TYPE: "token type not permitted",
	Parse.UNEXPECTED_TOKEN: "unexpected token",
	Parse.MISSING_DELIM: "expected delimiter",
	Parse.MISSING_ARGUMENT: "expected argument",
	Parse.EXPECTED_BRACE: "expected brace",
	Parse.UNCLOSED_BRACE: "brace never closed",
	Parse.EXPECTED_PAREN: "expected parenthesis",
	Parse.UNCLOSED_PAREN: "parenthesis never closed"
}


"""Types of semantic analysis errors."""
class Semantic(Enum):
	INVALID_TOKEN_TYPE = auto()
	INVALID_ARGUMENT = auto()
	INVALID_CONTEXT = auto()
	INCORRECT_ITEM_COUNT = auto()
	"""Human message format. For a compact format, use repr."""
	def __str__(self) -> str:
		return SEMANTIC_ENUM_STRS[self]

"""Pretty human strings for semantic analysis error types."""
SEMANTIC_ENUM_STRS = {
	Semantic.INVALID_TOKEN_TYPE: "token type not permitted",
	Semantic.INVALID_ARGUMENT: "invalid argument",
	Semantic.INVALID_CONTEXT: "invalid token context",
	Semantic.INCORRECT_ITEM_COUNT: "incorrect number of items"
}


"""Data structure for logging and reporing errors found during compilation."""
class CompileError:
	def __init__(self, id_:str, error:any, token:tokens.Token, message:str = ""):
		if DEBUG_ERRORS_AS_EXCEPTIONS:
			pos = token.pos.coords()
			err = f"Compile error {id_}: {error} at line {pos[0]}, column {pos[1]}"
			if message != "":
				err = f"{err}; {message}"
			raise RuntimeError(err)
		self.identifier = id_
		assert type(id_) == str
		self.error = error
		self.token = token
		self.message = message
	def __str__(self) -> str:
		pos = self.token.pos.coords()
		x = f"Error {self.identifier}: {self.error} at line {pos[0]}, column {pos[1]}"
		if self.message != "":
			x = f"{x}; {self.message}"
		return x
	def __repr__(self) -> str:
		return f"CompileError({self.identifier}, {self.error!r}, {self.token}, {self.message!r})"
