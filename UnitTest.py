import sys
import unittest
import errors

suites = []
results = []

errors.DEBUG_ERRORS_AS_EXCEPTIONS = False

def add_suite(name:str) -> None:
	suites.append(unittest.defaultTestLoader.loadTestsFromName(name))

if "parse" in sys.argv or "all" in sys.argv:
	add_suite("tests.testparse")
if "scan" in sys.argv or "all" in sys.argv:
	add_suite("tests.testscan")
if "semantics" in sys.argv or "all" in sys.argv:
	add_suite("tests.testsemantics")

for suite in suites:
	result = unittest.TextTestRunner().run(suite)
	results.append(result)

successes = 0
for result in results:
	successes += result.wasSuccessful()

print("=== TESTING COMPLETE ===")
print(successes, "of", len(results), "test suites passed")
