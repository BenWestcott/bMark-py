"""
@file      util.py
@copyright Benjamin Westcott, 2022
@brief     Useful general-purpose functions
"""

"""Indicates whether the given object has the requested method"""
def has_method(obj, method:str):
	return hasattr(obj, method) and callable(getattr(obj, method))

"""Indicates whether the given object has the requested data member"""
def has_data(obj, name:str):
	return hasattr(obj, name) and not callable(getattr(obj, name))

"""Like str(list), but calls str() on the elements instead of repr(), except for strings"""
def str_list(data:list) -> str:
	string_items = []
	for itm in data:
		strval = None
		if type(itm) == str:
			stritm = repr(itm)
		else:
			stritm = str(itm)
		string_items.append(stritm)
	return f"[{', '.join(string_items)}]"

"""Like str(dict), but sorted by keys and using str(value) instead of repr(value), except for strings"""
def str_dict_sorted(data:dict) -> str:
	keys = sorted(data.keys())
	if len(keys) == 0:
		return "{}"
	string_items = []
	for key in keys:
		val = data[key]
		strval = None
		if type(val) == str:
			strval = repr(val)
		else:
			strval = str(val)
		string_items.append(f"{key}: {strval}")
	return f"\{{{', '.join(string_items)}}}]"
