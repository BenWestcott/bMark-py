"""
@file      parse.py
@copyright Benjamin Westcott, 2022
@brief     Interface between the scanner token stream and the recursive descent parser
"""
import model
import tokens
import scan
import parsedata
import tree

"""Consumes tokens from a BufferedScanner and arranges them in a parse tree."""
class Parser:
	def __init__(self, config:model.Config, scn:scan.BufferedScanner, continue_:bool=False, debug:bool=False):
		self.config = config
		self.scan = scn
		self.debug = debug
		self.tree = tree.Tree(tree.Root)
		self.errors = []
		self.continue_ = continue_
	
	"""Print if debug flag is set"""
	def _dbg(self, msg:str) -> None:
		if self.debug:
			print("DEBUG: [PARSE] " + msg)
	
	"""Produces the parse tree. If returning false, the tree contains errors listed in the errors member."""
	def parse(self) -> bool:
		c = parsedata.ParseCtx(self.config, self.scan, self.errors, self.tree, self.continue_)
		ok = True
		while self.scan.has_next():
			consumed = self.scan.consumed
			ok &= parsedata.parse(c)
			# The parsing logic should always consume at least one token; only triggered if I forgot something
			assert self.scan.consumed != consumed
		assert ok == (len(self.errors)==0)
		return ok
