"""
@file      bMark.py
@copyright Benjamin Westcott, 2022
@brief     CLI for the bMark workflow
"""

import argparse
import sys

import tree
import model
import scan
import parse
import semantics

# Debug functions
"""Returns True if any double-links are mismatched"""
def find_broken_links(tr:tree.Tree) -> bool:
	if len(tr.children) == 0:
		return False
	broken = False
	for child in tr.children:
		broken |= find_broken_links(child)
		if id(child.parent) != id(tr):
			print("    between", tr.node, "and", child.node, file=sys.stderr)
			broken = True
	return broken

"""Returns True and prints any tree nodes that were not analyzed"""
def find_not_analyzed(tr:tree.Tree) -> bool:
	if (not tr.flags['A']) and (not tr.flags['C']):
		idx = tr.index()
		print(f"    -> {tr.node} ({idx}) in", sys.stderr)
		tr.flags['C'] = True
		return True
	if len(tr.children) == 0:
		return False
	for child in tr.children:
		if find_not_analyzed(child):
			if tr.node == tree.Root:
				print("    root node", sys.stderr)
			else:
				idx = child.index()
				print(f"    child {idx} ({tr.children[idx].str_shallow()}) of {tr.str_shallow()} in", sys.stderr)
			return True
	return False

# Argument parsing
parser = argparse.ArgumentParser(prog="bMark", description="CLI for bMark, a simple document markup language.")
parser.add_argument('-v', '--verbose', action='store_true', help="enable verbose logging to stderr")
parser.add_argument('-d', '--debug', action='store_true', help="enable debug logging to stderr")
parser.add_argument('-f', '--format', choices=['html'], help="force a specific output file format")
parser.add_argument('-c', '--config', nargs=1, default='config.xml', required=False, help="language specification file, defaults to ./config.xml")
parser.add_argument('input', nargs=1, help="bMark source document")
parser.add_argument('output', nargs=1, help="generated visual docment")
args = parser.parse_args()

# Argument interpretation and general initialization
if args.debug:
	args.verbose = True
if not args.format:
	if args.output[0].lower().endswith('.html') or args.output[0].lower().endswith('.htm'):
		if args.verbose:
			print("Automatically detected format: html", file=sys.stderr)
		args.format = 'html'
	else:
		print("Unable to determine output format; specify with --format", file=sys.stderr)
		exit(1)

configfile = open(args.config, 'r')
config = model.Config(configfile.read())
configfile.close()

# Scanner and parser initialization
infile = None
if not args.input:
	infile = sys.stdin
else:
	infile = open(args.input[0], 'r')
if args.debug:
	print("Opened input file:", infile, file=sys.stderr)
scn = scan.BufferedScanner(infile, config, 1, 1)
prs = parse.Parser(config, scn)
if args.debug:
	print("Created scanner:", scn, file=sys.stderr)
	print("Created parser:", prs, file=sys.stderr)

# Running the parser
if args.verbose:
	print("Parsing document...", file=sys.stderr)
success = prs.parse()
infile.close()
if args.verbose:
	print(f"Parse result: {success} with {len(prs.errors)} errors", file=sys.stderr)
if args.debug:
	out = open("ir_parse.xml", 'w')
	out.write(prs.tree.to_xml(pretty=True))
	out.close()
if not success:
	for err in prs.errors:
		print(err, file=sys.stderr)
	exit(1)
if args.debug:
	print("Scanning parse tree for broken links...")
	if find_broken_links(prs.tree):
		print("Error: broken parse tree", file=sys.stderr)
		exit(1)

# Running the semantic analyzer
if args.verbose:
	print("Analyzing parse tree...", file=sys.stderr)
ctx = semantics.SemanticsCTX(config, prs.tree, [])
if args.debug:
	print("Created semantics context", ctx, file=sys.stderr)
success = semantics.semantics(ctx)
if args.verbose:
	print(f"Semantic analysis result: {success} with {len(ctx.errors)} errors", file=sys.stderr)
if args.debug:
	out = open("ir_semantics.xml", 'w')
	out.write(prs.tree.to_xml(pretty=True))
	out.close()
if not success:
	for err in ctx.errors:
		print(err, file=sys.stderr)
	exit(1)
if args.debug:
	print("Scanning parse tree for missed nodes...", file=sys.stderr)
	unanalyzed = False
	while find_not_analyzed(prs.tree):
		unanalyzed = True
		print("    ---", file=sys.stderr)
	if unanalyzed:
		print("Error: semantic analysis missed some nodes", file=sys.stderr)
		exit(1)

# Running the emitter
outfile = None
if not args.output:
	outfile = sys.stdout
else:
	outfile = open(args.output[0], 'w')
if args.debug:
	print("Opened output file:", outfile, file=sys.stderr)
if args.format == 'html':
	if args.verbose:
		print("Emitting in HTML format", file=sys.stderr)
	import emit_html
	emit_html.emit(config, outfile, prs.tree)
outfile.close()
