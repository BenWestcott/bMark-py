#!/bin/sh
# Requires "coverage" python package; pip install --user coverage
coverage run UnitTest.py all
coverage html
xdg-open htmlcov/index.html
